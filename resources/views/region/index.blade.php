@extends('layout')

@section('title')
    Region Generator
@endsection

@section('content')
    <p>This generator creates a region from a fantasy world.</p>

    <form method="POST" action="/region">
        @csrf

        <div class="input-group">
            <p>What language should we use? If you pick "Constructed Language," the generator will create a language for
                you. If you pick "Any," a random language type will be chosen. Otherwise, the generator will use the
                specified language.</p>
            <select name="language-choice">
                <option value="any">Any</option>
                <option value="common">Common</option>
                <option value="conlang">Constructed Language</option>
            </select>
        </div>

        <div class="input-group">
            <p>What biome should this be? If you pick "Any," a random selection will be made.</p>
            <select name="biome">
                <option value="any">Any</option>
                @foreach ($biomes as $option)
                    <option value="{{ $option }}">{{ ucwords($option) }}</option>
                @endforeach
            </select>
        </div>

        <input type="submit" value="Generate New Region">
    </form>

    <h2>Most Recent Regions Generated</h2>

    @foreach ($regions as $region)
        <h3><a href="{{ route('region.show', ['guid' => $region->guid]) }}">{{ $region->name }}</a></h3>
        <p>{{ $region->description }}</p>
    @endforeach
@endsection
