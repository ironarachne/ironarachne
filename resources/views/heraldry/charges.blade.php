@extends('layout')

@section('title')
    All Charges
@endsection

@section('subtitle')
    All charges in the system
@endsection

@section('description')
    All charges in the system
@endsection

@section('content')
    <p>This is every charge currently in the system.</p>
    <div class="charges">
    @foreach($charges as $charge)
        <div class="charge">
            <img src="https://static.ironarachne.com/images/heraldry/sources/charges/{{ $charge->identifier }}-lines.png">
            <h3>{{ $charge->name }}</h3>
            <p>Tags:</p>
            @foreach($charge->tags as $tag) <span class="tag">{{ $tag->name }}</span>@endforeach
        </div>
    @endforeach
    </div>
@endsection
