@extends('layout')

@section('title')
    Heraldry Generator
@endsection

@section('subtitle')
    A coat-of-arms generator
@endsection

@section('content')
    <p>This generator creates a fictional coat-of-arms and accompanying blazon. All of the form fields are optional. If
        you leave them alone, you'll get a completely random coat-of-arms.</p>

    <form method="POST" action="{{ route('heraldry.create') }}">
        @csrf

        @if ($errors->any())
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        @endif

        <input type="submit" value="Just give me a random coat-of-arms">

        <h3>Field</h3>

        <p>The field is the main body of the coat-of-arms.</p>

        <img class="centered" src="{{ asset('img/field-types.png') }}">

        <div class="input-group">
            <label for="field_shape">Field Shape</label>
            <p>This is the kind of shield your coat-of-arms will be displayed on.</p>
            <select name="field_shape">
                <option value="any">Any</option>
                <option value="banner">Banner</option>
                <option value="engrailed">Engrailed</option>
                <option value="french">French</option>
                <option value="heater">Heater</option>
                <option value="square-eared">Square-Eared</option>
                <option value="wedge">Wedge</option>
            </select>
        </div>

        <h3>Charges</h3>

        <p>Charges are the symbols in the foreground of the coat-of-arms. You can view a <a
                href="{{ route('heraldry.charges') }}">list of all charges</a>.</p>

        <div class="input-group">
            <label for="have_charges">Have Charges?</label>
            <p>Do you want charges? You can opt to include them, exclude them, or leave it to chance ("any").</p>
            <select name="have_charges">
                <option value="any">Any</option>
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
        </div>

        <div class="input-group">
            <label for="charge_tag">Charge Tag</label>
            <p>Enter a word (just one) to describe what kind of charge you want. If the system can't find a matching
                charge, it will give you a random one. If you leave this blank, you'll also get a random one.</p>
            <input type="text" name="charge_tag" placeholder="any">
        </div>

        <h3>Division of the Field</h3>

        <p>The division of the field is how the background is split into sections.</p>

        <img class="centered" src="{{ asset('img/divisions.png') }}">

        <div class="input-group">
            <label for="division">Division of the Field</label>
            <p>This is the way the background will be divided up.</p>
            <select name="division">
                <option value="any">Any</option>
                <option value="plain">No Division</option>
                <option value="pale">Per Pale</option>
                <option value="fess">Per Fess</option>
                <option value="bend">Per Bend</option>
                <option value="bend-sinister">Per Bend Sinister</option>
                <option value="saltire">Per Saltire</option>
                <option value="chevron">Per Chevron</option>
                <option value="quarterly">Quarterly</option>
            </select>
        </div>

        <h3>Variations of the Field</h3>

        <p>Variations of the field are patterns applied to sections of the division of the field.</p>

        <img class="centered" src="{{ asset('img/variations.png') }}">

        <div class="input-group">
            <label for="variation_1">First Variation of the Field</label>
            <p>This is the pattern that will be applied first to the division.</p>
            <select name="variation_1">
                <option value="any">Any</option>
                <option value="plain">Plain</option>
                <option value="barry">Barry</option>
                <option value="bendy">Bendy</option>
                <option value="bendy-sinister">Bendy Sinister</option>
                <option value="paly">Paly</option>
                <option value="chequy">Chequy</option>
            </select>
        </div>

        <div class="input-group">
            <label for="variation_2">Second Variation of the Field</label>
            <p>This is the pattern that will be applied second to the division.</p>
            <select name="variation_2">
                <option value="any">Any</option>
                <option value="plain">Plain</option>
                <option value="barry">Barry</option>
                <option value="bendy">Bendy</option>
                <option value="bendy-sinister">Bendy Sinister</option>
                <option value="paly">Paly</option>
                <option value="chequy">Chequy</option>
            </select>
        </div>

        <h3>Tinctures</h3>

        <p>Tinctures are the colors or, in the case of furs, patterns applied to the variations or charges.</p>

        <div class="columns">
            <div class="column half">
                <div class="input-group">
                    <label for="primary_tincture">Charge Tincture</label>
                    <p>Choose a primary tincture. This will be used for your charges.</p>
                    <select name="primary_tincture">
                        <option value="any">Any</option>
                        <optgroup label="Colors">
                            <option value="gules">Gules (red)</option>
                            <option value="azure">Azure (blue)</option>
                            <option value="vert">Vert (green)</option>
                            <option value="sable">Sable (black)</option>
                            <option value="purpure">Purpure (purple)</option>
                        </optgroup>
                        <optgroup label="Metals">
                            <option value="Or">Or (gold)</option>
                            <option value="argent">Argent (white)</option>
                        </optgroup>
                        <optgroup label="Stains">
                            <option value="murrey">Murrey (wine)</option>
                            <option value="sanguine">Sanguine (blood)</option>
                            <option value="tenne">Tenné (tan)</option>
                        </optgroup>
                        <optgroup label="Furs">
                            <option value="ermine">Ermine</option>
                            <option value="erminois">Erminois</option>
                            <option value="ermines">Ermines</option>
                            <option value="pean">Pean</option>
                        </optgroup>
                    </select>
                </div>

                <p>Now choose your background tinctures. Tinctures 1 and 2 will be applied to your first variation, and
                    tinctures 3 and 4 will be applied to your second.</p>

                @for($i=1;$i<5;$i++)
                    <div class="input-group">
                        <label for="bg_tincture_{{ $i }}">Background Tincture {{ $i }}</label>
                        <p>Choose a background tincture. This will be used for your field. Note: this is dependent on
                            your
                            division and variations.</p>
                        <select name="bg_tincture_{{ $i }}">
                            <option value="any">Any</option>
                            <optgroup label="Colors">
                                <option value="gules">Gules (red)</option>
                                <option value="azure">Azure (blue)</option>
                                <option value="vert">Vert (green)</option>
                                <option value="sable">Sable (black)</option>
                                <option value="purpure">Purpure (purple)</option>
                            </optgroup>
                            <optgroup label="Metals">
                                <option value="Or">Or (gold)</option>
                                <option value="argent">Argent (white)</option>
                            </optgroup>
                            <optgroup label="Stains">
                                <option value="murrey">Murrey (wine)</option>
                                <option value="sanguine">Sanguine (blood)</option>
                                <option value="tenne">Tenné (tan)</option>
                            </optgroup>
                            <optgroup label="Furs">
                                <option value="ermine">Ermine</option>
                                <option value="erminois">Erminois</option>
                                <option value="ermines">Ermines</option>
                                <option value="pean">Pean</option>
                            </optgroup>
                        </select>
                    </div>
                @endfor</div>
            <div class="column half"><p>Colors:</p>
                <div class="tinctures">
                    <div class="gules"><span>gules</span></div>
                    <div class="vert"><span>vert</span></div>
                    <div class="purpure"><span>purpure</span></div>
                    <div class="azure"><span>azure</span></div>
                    <div class="sable"><span>sable</span></div>
                </div>
                <p>Metals:</p>
                <div class="tinctures">
                    <div class="or"><span>Or</span></div>
                    <div class="argent"><span>argent</span></div>
                </div>
                <p>Stains:</p>
                <div class="tinctures">
                    <div class="murrey"><span>murrey</span></div>
                    <div class="sanguine"><span>sanguine</span></div>
                    <div class="tenné"><span>tenné</span></div>
                </div>
                <p>Furs:</p>
                <div class="tinctures">
                    <div class="ermine"><span>ermine</span></div>
                    <div class="ermines"><span>ermines</span></div>
                    <div class="erminois"><span>erminois</span></div>
                    <div class="pean"><span>pean</span></div>
                </div>
            </div>
        </div>


        <input type="submit" value="Generate">
    </form>

    <h2>Most Recent Heraldry Generated</h2>
    @foreach ($devices as $device)
        <article class="heraldry-list">
            <img src="{{ $device->url }}">
            <p>
                <a href="{{ route('heraldry.show', ['guid' => $device->guid ]) }}">{{ $device->blazon }}</a> @if(isset($device->user->name))
                    by {{ $device->user->name }}@endif</p>
        </article>
    @endforeach
@endsection
