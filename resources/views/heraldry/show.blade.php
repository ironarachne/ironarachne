@extends('layout')

@section('title')
    Generated Heraldry
@endsection

@section('subtitle')
    {{ $heraldry->blazon }}
@endsection

@section('description')
    {{ $heraldry->blazon }}
@endsection

@section('image-metadata', $heraldry->url)

@section('content')
    @if(Auth::user())
        @if(empty($heraldry->owner))
            <div>
                <p>This heraldry is unclaimed. If you'd like, you can set this as your personal heraldry.</p>
                <form method="POST"
                      action="{{ route('users.set-heraldry', ['user' => Auth::user(), 'heraldry' => $heraldry]) }}">
                    @csrf
                    <input type="submit" value="Set Heraldry">
                </form>
            </div>
        @elseif($heraldry->owner->id == Auth::user()->id)
            <div>
                <p>This is your personal heraldry.</p>
            </div>
        @else
            <div>
                <p>This heraldry is the personal arms of <a href="{{ route('users.show', ['user' => $heraldry->owner]) }}">{{ $heraldry->owner->name }}</a>.</p>
            </div>
        @endif
    @endif

    <div class="heraldry-large">
        <p>{{ $heraldry->blazon }}</p>
        <img src="{{ $heraldry->url }}" alt="{{ $heraldry->blazon }} coat of arms">
    </div>
    <div class="pennon">
        <img src="{{ $pennonURL }}">
    </div>
@endsection
