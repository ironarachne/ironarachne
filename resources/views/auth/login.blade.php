@extends('layout')

@section('title')
    Login
@endsection

@section('content')
    <p>Log in to your user account.</p>

    <form method="POST" action="/login">
        @csrf

        @if ($errors->any())
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        @endif

        <div class="input-group">
            <label for="email">Email Address</label>
            <input type="email" name="email">
        </div>

        <div class="input-group">
            <label for="password">Password</label>
            <input type="password" name="password">
        </div>

        <input type="submit" value="Login">

    </form>

    <p><a href="{{ route('password.request') }}">Forgot your password?</a></p>
@endsection
