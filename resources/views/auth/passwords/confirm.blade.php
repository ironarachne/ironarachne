@extends('layout')

@section('title')
    Confirm Password
@endsection

@section('content')
    <h2>Confirm Password</h2>

    @if ($errors->any())
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif

    <form method="POST" action="{{ route('password.confirm') }}">
        @csrf
        <div class="input-group">
            <label for="password">Password</label>
            <input type="password" name="password">
        </div>

        <input type="submit" value="Confirm Password">
    </form>
    <a class="button" href="{{ route('password.request') }}">Forgot your password?</a>
@endsection
