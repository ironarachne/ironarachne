@extends('layout')

@section('title')
    Password Reset
@endsection

@section('content')
    <h2>Request a Password Reset</h2>

    @if ($errors->any())
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="input-group">
            <label for="email">Email Address</label>
            <input type="email" name="email">
        </div>

        <input type="submit" value="Send password reset link">
    </form>
@endsection
