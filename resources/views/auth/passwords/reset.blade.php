@extends('layout')

@section('title')
    Password Reset
@endsection

@section('content')
    <h2>Reset Password</h2>

    @if ($errors->any())
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif

    <form method="POST" action="{{ route('password.update') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="input-group">
            <label for="email">Email Address</label>
            <input type="email" name="email">
        </div>

        <div class="input-group">
            <label for="password">Password</label>
            <input type="password" name="password">
        </div>

        <div class="input-group">
            <label for="password_confirmation">Confirm Password</label>
            <input type="password" name="password_confirmation">
        </div>

        <input type="submit" value="Reset Password">
    </form>
@endsection
