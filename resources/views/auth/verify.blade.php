@extends('layout')

@section('title')
    Verification Sent
@endsection

@section('content')
    <h1>Verification Sent</h1>

    <form method="POST" action="{{ route('verification.resend') }}">
        @csrf
        <input type="submit" value="Click to request another verification email">
    </form>
@endsection
