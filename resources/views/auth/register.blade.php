@extends('layout')

@section('title')
    Register a New Account
@endsection

@section('content')
    <p>Register a new user account.</p>

    <form method="POST" action="/register">
        @csrf

        @if ($errors->any())
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        @endif

        <div class="input-group">
            <label for="name">Display Name</label>
            <input type="text" name="name">
        </div>

        <div class="input-group">
            <label for="email">Email Address</label>
            <input type="email" name="email">
        </div>

        <div class="input-group">
            <label for="password">Password</label>
            <input type="password" name="password">
        </div>

        <div class="input-group">
            <label for="password_confirmation">Confirm Password</label>
            <input type="password" name="password_confirmation">
        </div>

        <input type="submit" value="Register">
    </form>

@endsection
