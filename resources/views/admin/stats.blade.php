@extends('layout')

@section('title')
    Statistics
@endsection

@section('subtitle')
    Site statistics
@endsection

@section('content')
    <h2>Site Statistics</h2>

    <p>Culture Count: {{ number_format($counts['culture']) }}</p>
    <p>Heraldry Count: {{ number_format($counts['heraldry']) }}</p>
    <p>Region Count: {{ number_format($counts['region']) }}</p>
    <p>User Count:: {{ number_format($counts['user']) }}</p>
@endsection
