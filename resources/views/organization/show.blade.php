@extends('layout')

@section('title')
    {{ ucfirst($organization->name) }}
@endsection

@section('subtitle')
    {{ $organization->description }}
@endsection

@section('description')
    {{ $organization->description }}
@endsection

@section('content')
    {!! $organization->html !!}
@endsection
