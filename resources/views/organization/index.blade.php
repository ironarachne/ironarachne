@extends('layout')

@section('title')
    Organization Generator
@endsection

@section('subtitle')
    A generator of fantasy organizations
@endsection

@section('description')
    This tool procedurally generates fantasy organizations
@endsection

@section('content')
    <p>This generator creates a fictional organization from a fantasy world.</p>

    <form method="POST" action="{{ route('organization.create') }}">
        @csrf
        <div class="input-group">
            <label for="type">Type of Organization</label>
            <select name="type">
                <option value="random">Random</option>
                <option value="artisan guild">Artisan Guild</option>
                <option value="adventuring company">Adventuring Company</option>
                <option value="holy order">Holy Order</option>
                <option value="mercenary company">Mercenary Company</option>
                <option value="wizard school">Wizard School</option>
            </select>
        </div>
        <input type="submit" value="Generate New Organization">
    </form>

    <h2>Most Recent Organizations Generated</h2>
    @foreach ($organizations as $organization)
        <h3><a href="{{ route('organization.show', ['guid' => $organization->guid]) }}">{{ ucfirst($organization->name) }}</a>  @if(isset($organization->user->name))
                by {{ $organization->user->name }}@endif
        </h3>
        <p>{{ ucfirst($organization->description) }}</p>
    @endforeach
@endsection
