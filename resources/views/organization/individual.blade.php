<div class="banner">
    <img src="{{ $organization->heraldry->image_url }}">
    <h2>{{ ucfirst($organization->name) }}</h2>
</div>

<p>{{ $organization->description }}</p>

<img src="{{ $pennonURL }}">

<h3>Leadership</h3>

@foreach($organization->leaders as $leader)
    <div>
        <h4>{{ $leader->primary_title }} {{ $leader->first_name }} {{ $leader->last_name }}</h4>
        <p>{{ $leader->description }}</p>
    </div>
@endforeach

<h3>Notable Members</h3>

@foreach($organization->notable_members as $member)
    <div>
        <h4>{{ $member->first_name }} {{ $member->last_name }}</h4>
        <p>{{ $member->description }}</p>
    </div>
@endforeach

@if(sizeof($organization->factions) > 0)
<h3>Factions</h3>

@foreach($organization->factions as $faction)
    <div>
        <p>{{ $faction->description }}</p>
    </div>
@endforeach
@endif

