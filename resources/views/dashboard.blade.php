@extends('layout')

@section('title')
    {{ Auth::user()->name }}'s Dashboard
@endsection

@section('subtitle')
    Your personal dashboard
@endsection

@section('content')
    <p>This is your dashboard. Your creations appear here.</p>

    <p><a href="{{ route('users.show', ['user' => Auth::user()]) }}">View your profile</a></p>

    @if(Auth::user()->is_admin)
        <h2><img class="art-icon" src="{{ asset('img/art-icons/admin_gear.png') }}"> Administration</h2>

        <ul>
            <li><a href="{{ route('users.index') }}">List of Users</a></li>
            <li><a href="{{ route('admin.stats') }}">Statistics</a></li>
        </ul>
    @endif

    <h2><img class="art-icon" src="{{ asset('img/art-icons/dashboard_creations.png') }}"> Your Creations</h2>

    <h3>Cultures</h3>

    <ul>
        @foreach ($cultures as $culture)
            <li><a href="{{ route('culture.show', ['guid' => $culture['guid']]) }}">{{ $culture->name }}</a> - {{ $culture->description }}</li>
        @endforeach
    </ul>

    <h3>Regions</h3>

    <ul>
        @foreach ($regions as $region)
            <li><a href="{{ route('region.show', ['guid' => $region['guid']]) }}">{{ $region->name }}</a> - {{ $region->description }}</li>
        @endforeach
    </ul>

    <h3>Heraldry</h3>

    @foreach ($devices as $device)
        <article class="heraldry-list">
            <img src="{{ $device->url }}">
            <p><a href="{{ route('heraldry.show', ['guid' => $device->guid ]) }}">{{ $device->blazon }}</a></p>
        </article>
    @endforeach
@endsection
