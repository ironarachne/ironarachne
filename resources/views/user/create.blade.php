@extends('layout')

@section('title')
    Create User
@endsection

@section('description')
    Create a new user
@endsection

@section('content')
    <h2>Create User</h2>

    @if ($errors->any())
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif

    <form method="POST" action="{{ route('users.store') }}">
        @csrf
        <div class="input-group">
            <label for="name">Display Name</label>
            <input type="text" name="name">
        </div>

        <div class="input-group">
            <label for="email">Email Address</label>
            <input type="email" name="email">
        </div>

        <div class="input-group">
            <label for="password">Password</label>
            <input type="password" name="password">
        </div>

        <div class="input-group">
            <label for="password_confirmation">Confirm Password</label>
            <input type="password" name="password_confirmation">
        </div>

        <input type="submit" value="Create">
    </form>
@endsection
