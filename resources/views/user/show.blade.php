@extends('layout')

@section('title')
    {{ $user->name }}'s Profile
@endsection

@section('description')
    {{ $user->name }}'s User Profile
@endsection

@section('content')
    <h2>{{ $user->name }}'s Profile</h2>

    @if ($errors->any())
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif

    @if(!empty(Auth::user()->heraldry->url))
        <img src="{{ Auth::user()->heraldry->url }}">
    @endif

    @if(Auth::user()->is_admin)
        <p>Email Address: {{ $user->email }}</p>
        <p>Created {{ date('F j, Y, g:i a', strtotime($user->created_at)) }}</p>
    @endif

    @if(Auth::user()->id == $user->id)
        <p>This is you!</p>
        <p>Your email: {{ $user->email }}</p>
        <a href="{{ route('users.edit', ['user' => $user]) }}" class="button">Edit</a>
    @endif
@endsection
