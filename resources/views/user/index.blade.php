@extends('layout')

@section('title')
    Users
@endsection

@section('description')
    All users
@endsection

@section('content')
    <h2>Users</h2>

    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Heraldry</th>
                <th>Regions</th>
                <th>Cultures</th>
                <th>Created</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td><a href="{{ route('users.show', ['user' => $user]) }}">{{ $user->name }}</a></td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->heraldries->count() }}</td>
                <td>{{ $user->regions->count() }}</td>
                <td>{{ $user->cultures->count() }}</td>
                <td>{{ date('Y-m-d', strtotime($user->created_at)) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $users->links() }}
@endsection
