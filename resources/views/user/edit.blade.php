@extends('layout')

@section('title')
    Edit User {{ $user->name }}
@endsection

@section('description')
    Edit {{ $user->name }}
@endsection

@section('content')
    <h2>Editing User</h2>

    @if ($errors->any())
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif

    <form method="POST" action="{{ route('users.update', ['user' => $user]) }}">
        @method('PUT')
        @csrf
        <div class="input-group">
            <label for="name">Display Name</label>
            <input type="text" name="name" value="{{ $user->name }}">
        </div>

        <div class="input-group">
            <label for="email">Email Address</label>
            <input type="email" name="email" value="{{ $user->email }}">
        </div>

        <input type="submit" value="Save Changes">
    </form>

    <p><a href="{{ route('password.request') }}">Need to change your password?</a></p>
@endsection
