@extends('layout')

@section('title')
    Iron Arachne
@endsection

@section('subtitle')
    Procedural Generation Tools for Tabletop Role-playing Games
@endsection

@section('description')
    Procedural Generation Tools for Tabletop Role-playing Games
@endsection

@section('content')
    <p>This website hosts tools intended to make life easier for players of tabletop RPGs. It's a living website,
        and will continue to receive new tools on a semiregular basis.</p>
    <p>My name is <a href="https://benovermyer.com">Ben Overmyer</a>. I'm the developer behind this site.</p>

    <p>Lastly, if you find this site useful, consider buying me a coffee. The server costs are not cheap, and I fund the site myself.</p>
    <script type="text/javascript" src="https://cdnjs.buymeacoffee.com/1.0.0/button.prod.min.js" data-name="bmc-button" data-slug="benovermyer" data-color="#000000" data-emoji=""  data-font="Cookie" data-text="Buy me a coffee" data-outline-color="#fff" data-font-color="#fff" data-coffee-color="#fd0" ></script>

    @foreach($posts as $post)
        <article class="post">
            <h2><img class="art-icon" src="{{ asset('img/art-icons/blog_post.png') }}"> {{ $post['title'] }}</h2>
            <p class="date">Published {{ date('l F jS, Y', strtotime($post['created'])) }}</p>
            <div>{!! $post['body'] !!}</div>
        </article>
    @endforeach
@endsection
