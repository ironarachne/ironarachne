@extends('layout')

@section('title')
    Culture Generator
@endsection

@section('subtitle')
    A generator of fantasy cultures
@endsection

@section('description')
    This tool procedurally generates fantasy cultures
@endsection

@section('content')
    <p>This generator creates a fictional culture from a fantasy world.</p>

    <form method="POST" action="/culture">
        @csrf
        <div class="input-group">
            <p>What language should we use? If you pick "Constructed Language," the generator will create a language for
                you. If you pick "Any," a random language type will be chosen. Otherwise, the generator will use the
                specified language.</p>
            <select name="language-choice">
                <option value="any">Any</option>
                <option value="common">Common</option>
                <option value="conlang">Constructed Language</option>
            </select>
        </div>

        <div class="input-group">
            <p>What biome should they be from? If you pick "Any," a random selection will be made.</p>
            <select name="biome">
                <option value="any">Any</option>
                @foreach ($biomeOptions as $option)
                    <option value="{{ $option }}">{{ ucwords($option) }}</option>
                @endforeach
            </select>
        </div>

        <input type="submit" value="Generate New Culture">
    </form>

    <h2>Most Recent Cultures Generated</h2>
    @foreach ($cultures as $culture)
        <h3><a href="{{ route('culture.show', ['guid' => $culture->guid]) }}">The {{ $culture->name }} Culture</a>
        </h3>
        <p>{{ $culture->description }}</p>
    @endforeach
@endsection
