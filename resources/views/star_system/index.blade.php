@extends('layout')

@section('title')
    Star System Generator
@endsection

@section('content')
    <p>This generates random star systems.</p>

    <div id="app">
        <div class="input-group">
            <label>Seed</label>
            <input type="text" v-model="seed">
        </div>

        <button v-on:click="generate" :disabled="loaded === false">Generate</button>

        <p v-show="loaded === false">Generating...</p>

        <div v-show="loaded">
            <h2>The @{{ star_system.name }} System</h2>

            <p>@{{ star_system.description }}</p>

            <h3>Stars</h3>

            <div v-for="star in star_system.stars" class="star">
                <img v-bind:src="star.image_url">
                <h4>@{{ star.name }}</h4>
                <p>@{{ star.type.name }} star</p>
            </div>

            <h3>Planets</h3>

            <div v-for="planet in star_system.planets" class="planet">
                <img v-bind:src="planet.image_url">
                <h4>@{{ planet.name }}</h4>
                <p>a @{{ planet.size }} @{{ planet.planet_type.name }} planet</p>
            </div>
        </div>

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.min.js"></script>
    <script type="text/javascript">
        let app = new Vue({
            el: '#app',
            data: {
                loaded: false,
                seed: '',
                star_system: {
                    name: '',
                    description: '',
                    stars: [],
                    planets: []
                }
            },
            methods: {
                generate: function(event) {
                    this.loaded = false

                    if (this.seed === '') {
                        this.seed = Math.floor(Math.random() * Math.floor(99999999999)).toString()
                    }

                    url = '{{ route('api.star_system.random') }}'

                    fetch(url + '/' + this.seed)
                    .then(response => response.json())
                    .then(response => {
                        this.loaded = true
                        this.star_system = response.star_system
                    })
                    .catch(error => console.error(error))
                }
            }
        })

        app.generate()
    </script>
@endsection
