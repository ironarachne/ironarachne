<?php


namespace App;


class Goal
{
    public string $name;
    public string $description_template;
    public array $compatible_traits;
    public array $incompatible_traits;

    public function describe(string $name, string $gender): string
    {
        $description = str_replace('{{Name}}', $name, $this->description_template);
        $description = str_replace('{{PossessivePronoun}}', pronoun($gender, 'possessive'), $description);

        return $description;
    }

    public static function random(array $possibleGoals): Goal
    {
        return random_item($possibleGoals);
    }
}
