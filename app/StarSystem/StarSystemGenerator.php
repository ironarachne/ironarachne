<?php


namespace App\StarSystem;


use App\Planet\PlanetGenerator;
use App\Star\StarGenerator;
use Romans\Filter\IntToRoman;

class StarSystemGenerator
{
    public function generate(): StarSystem
    {
        $system = new StarSystem();
        $system->stars = $this->randomStars();
        $system->planets = $this->randomPlanets();
        $system->name = $this->randomName();

        $count = 1;
        $filter = new IntToRoman();
        foreach ($system->planets as $planet) {
            $planet->name = $system->name . ' ' . $filter->filter($count);
            $count++;
        }

        if (sizeof($system->stars) == 1) {
            $system->stars[0]->name = $system->name;
        } else {
            $count = 1;
            foreach ($system->stars as $star) {
                if ($count == 1) {
                    $countWord = 'Alpha';
                } elseif ($count == 2) {
                    $countWord = 'Beta';
                } else {
                    $countWord = 'Delta';
                }

                $star->name = $system->name . ' ' . $countWord;
                $count++;
            }
        }

        $system->description = $this->describe($system);

        return $system;
    }

    private function describe(StarSystem $system): string
    {
        $description = 'The ' . $system->name . ' system';
        if (sizeof($system->stars) == 1) {
            $description .= ' has a single ' . $system->stars[0]->type->name . ' star';
        } elseif (sizeof($system->stars) == 2) {
            $description .= ' is a binary system';
        } else {
            $description .= ' is a trinary system';
        }

        $countOfPlanets = sizeof($system->planets);

        $description .= ' with ' . $countOfPlanets . ' planets.';

        return $description;
    }

    private function randomName(): string
    {
        $prefixes = [
            'Ar',
            'Ab',
            'Al',
            'Meb',
            'Pher',
            'Sched',
            'Tay',
            'Ser',
            'San',
            'Dor',
            'Kan',
            'Kel',
            'Mep',
            'Frin',
            'Vuat',
            'Qin',
            'Zor',
        ];

        $suffixes = [
            'o',
            'ao',
            'an',
            'am',
            'un',
            'uy',
            'to',
            'va',
            'du',
            'ado',
            'ano',
            'ast',
            'est',
            'il',
            'gro',
            'shec',
            'sheb',
            'rep',
            'aab',
            'oic',
        ];

        $prefix = random_item($prefixes);
        $suffix = random_item($suffixes);

        return $prefix . $suffix;
    }

    private function randomPlanets(): array
    {
        $planets = [];

        $innerPlanetTypes = [
            'arid',
            'barren',
            'desert',
            'volcanic',
        ];

        $outerPlanetTypes = [
            'arid',
            'barren',
            'desert',
            'gas giant',
            'ice',
        ];

        $lifeBearingTypes = [
            'garden',
            'jungle',
            'ocean',
        ];

        $countOfLifeBearing = 0;
        $lifeBearingCap = mt_rand(1, 2);

        $numberOfPlanets = mt_rand(3, 7);

        $gen = new PlanetGenerator();

        for ($i = 0; $i < $numberOfPlanets; $i++) {
            if ($i > 2 && $countOfLifeBearing < $lifeBearingCap) {
                $randomType = random_item($lifeBearingTypes);
                $countOfLifeBearing++;
            } elseif ($i < 4) {
                $randomType = random_item($innerPlanetTypes);
            } else {
                $randomType = random_item($outerPlanetTypes);
            }

            $planets [] = $gen->generate($randomType, 'random', 128, 128);
        }

        return $planets;
    }

    private function randomStars(): array
    {
        $stars = [];

        $weightedNumbers = [
            1 => 50,
            2 => 5,
            3 => 1,
        ];

        $number = random_weighted_item($weightedNumbers);
        $starGen = new StarGenerator();

        for ($i = 0; $i < $number; $i++) {
            $stars [] = $starGen->generate('random', 128, 128);
        }

        return $stars;
    }
}
