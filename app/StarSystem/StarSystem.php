<?php


namespace App\StarSystem;


class StarSystem
{
    public string $name;
    public string $description;
    public array $planets;
    public array $stars;

    public function __construct() {
        $this->planets = [];
        $this->stars = [];
    }
}
