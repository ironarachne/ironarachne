<?php


namespace App\Planet;


use App\Math\PerlinNoise;
use Imagick;
use Intervention\Image\Image;

class BarrenPlanetType extends PlanetType
{
    public function __construct()
    {
        $this->name = 'barren';
        $this->cloud_density = 0;
        $this->has_atmosphere = false;
        $this->has_clouds = false;
        $this->has_polar_icecaps = false;
        $this->size_small_min = 0.2;
        $this->size_small_max = 0.3;
        $this->size_medium_min = 0.3;
        $this->size_medium_max = 0.6;
        $this->size_large_min = 0.6;
        $this->size_large_max = 1.0;
    }

    public function getTexture(int $width = 512, int $height = 512): Image
    {
        $noiseGen = new PerlinNoise($width, $height);
        $elevation = $noiseGen->generate();

        $img = new Imagick();
        $img->newImage($width, $height, 'rgb(0,0,0)');

        $iterator = $img->getPixelIterator();

        foreach ($iterator as $row => $pixels) {
            foreach ($pixels as $col => $pixel) {
                $h = (155 * $elevation[$row][$col]) + 25;
                $pixel->setColor("rgb($h, $h, $h)");
            }
            $iterator->syncIterator();
        }

        return \Intervention\Image\Facades\Image::make($img);
    }
}
