<?php


namespace App\Planet;


use App\Math\PerlinNoise;
use Imagick;
use Intervention\Image\Facades\Image;

class GardenPlanetType extends PlanetType
{
    public function __construct()
    {
        $this->name = 'garden';
        $this->cloud_density = 60;
        $this->has_atmosphere = true;
        $this->has_clouds = true;
        $this->has_polar_icecaps = true;
        $this->size_small_min = 0.5;
        $this->size_small_max = 0.6;
        $this->size_medium_min = 0.6;
        $this->size_medium_max = 0.8;
        $this->size_large_min = 0.8;
        $this->size_large_max = 1.0;
    }

    public function getTexture(int $width = 512, int $height = 512): \Intervention\Image\Image
    {
        $noiseGen = new PerlinNoise($width, $height);

        $elevation = $noiseGen->generate();
        $moisture = $noiseGen->generate();
        $temperature = $noiseGen->generate();

        $img = new Imagick();
        $img->newImage($width, $height, 'rgb(0,0,0)');

        $iterator = $img->getPixelIterator();

        foreach($iterator as $row => $pixels) {
            foreach($pixels as $col => $pixel) {
                $h = 255 * $elevation[$row][$col];
                $m = 255 * $moisture[$row][$col];
                $t = 255 * (($height - abs($row - ($height / 2))) / $height) + ($temperature[$row][$col] / 2);

                $color = $this->biome($h, $m, $t);

                $pixel->setColor($color);
            }
            $iterator->syncIterator();
        }

        return Image::make($img);
    }

    private function biome(float $elevation, float $moisture, float $temperature): string
    {
        if ($elevation < 50) {
            return 'rgb(15, 28, 142)'; // deep ocean
        }

        if ($elevation < 125) {
            return 'rgb(24, 37, 158)'; // ocean
        }

        $r = 0;
        $g = 0;
        $b = 0;

        // adjust for moisture
        $moistureRatio = $moisture / 255;
        $g += (100 * $moistureRatio);
        $b += (50 * $moistureRatio);

        // adjust for temperature
        $temperatureRatio = $temperature / 255;
        $r += (100 * $temperatureRatio);
        $g += (90 * $temperatureRatio);

        // adjust for elevation
        $saturation = $elevation / 255;
        $r += ($elevation - $r) * $saturation;
        $g += ($elevation - $g) * $saturation;
        $b += ($elevation - $b) * $saturation;

        return "rgb($r, $g, $b)";
    }
}
