<?php


namespace App\Planet;


use Intervention\Image\Image;

abstract class PlanetType
{
    public string $name;
    public int $cloud_density;
    public bool $has_atmosphere;
    public bool $has_clouds;
    public bool $has_polar_icecaps;
    public float $size_small_min;
    public float $size_small_max;
    public float $size_medium_min;
    public float $size_medium_max;
    public float $size_large_min;
    public float $size_large_max;

    abstract public function getTexture(int $width, int $height): Image;
}
