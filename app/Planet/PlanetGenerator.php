<?php


namespace App\Planet;


use App\Graphics\StarField;
use App\Math\PerlinNoise;
use Illuminate\Support\Facades\Storage;
use Imagick;
use ImagickDraw;
use ImagickPixel;
use Intervention\Image\Facades\Image;


class PlanetGenerator
{
    public function generate(string $type = 'random', string $size = 'random', int $imageWidth = 128, int $imageHeight = 128): Planet
    {
        if ($type == 'random') {
            $type = $this->randomType();
        } else {
            $type = $this->getTypeByName($type);
        }

        if ($size == 'random') {
            $size = random_item(['small', 'medium', 'large']);
        }

        $smallestDimension = min($imageWidth, $imageHeight);

        if ($size == 'small') {
            $min = floor($type->size_small_min * $smallestDimension);
            $max = floor($type->size_small_max * $smallestDimension);
        } elseif ($size == 'medium') {
            $min = floor($type->size_medium_min * $smallestDimension);
            $max = floor($type->size_medium_max * $smallestDimension);
        } else {
            $min = floor($type->size_large_min * $smallestDimension);
            $max = floor($type->size_large_max * $smallestDimension);
        }

        $planetSize = mt_rand($min, $max);

        $image = $this->render($type, $imageWidth, $imageHeight, $planetSize);

        $id = md5($type->name . microtime());
        $fileName = "$id.png";
        $path = 'images/planets/' . date('Y/m/d');

        Storage::cloud()->put("$path/$fileName", (string)$image->encode('png'), 'public');
        $imageURL = "https://static.ironarachne.com/$path/$fileName";

        return new Planet($type->name, $type, $imageURL, $size);
    }

    private function getAllTypes(): array {
        return [
            new AridPlanetType(),
            new BarrenPlanetType(),
            new DesertPlanetType(),
            new GardenPlanetType(),
            new GasGiantPlanetType(),
            new IcePlanetType(),
            new JunglePlanetType(),
            new OceanPlanetType(),
            new VolcanicPlanetType(),
        ];
    }

    private function getTypeByName(string $name): PlanetType
    {
        $types = $this->getAllTypes();

        foreach ($types as $type) {
            if ($type->name == $name) {
                return $type;
            }
        }
    }

    private function randomType(): PlanetType
    {
        $types = $this->getAllTypes();

        return random_item($types);
    }

    private function render(PlanetType $type, int $width = 512, int $height = 512, int $planetSize = 450): \Intervention\Image\Image
    {
        $canvas = Image::canvas($width, $height);

        $texture = $type->getTexture($width, $height);

        if ($type->has_polar_icecaps) {
            $iceCaps = $this->generateIceCaps($width, $height);
            $texture->insert($iceCaps);
        }

        $imagick = $texture->getCore();
        $controlPoints = [0.4, 0.0, 0.0, 0.8];
        $imagick->distortImage(Imagick::DISTORTION_BARREL, $controlPoints, true);
        $texture = Image::make($imagick);

        $planetMask = Image::canvas($width, $height);
        $planetMask->circle($planetSize, $width / 2, $height / 2, function ($draw) {
            $draw->background('#000000');
        });

        if ($type->has_clouds) {
            $cloudImage = $this->generateClouds($planetSize, $width, $height, $type->cloud_density);
            $texture->insert($cloudImage, 'center', $width / 2, $height / 2);
        }

        $shadowImage = $this->generateShadow($planetSize, $width, $height);
        $texture->insert($shadowImage, 'center', $width / 2, $height / 2);

        $backlightImage = $this->generateBacklight($planetSize, $width, $height);
        $texture->insert($backlightImage, 'center', $width / 2, $height / 2);

        $texture->mask($planetMask, true);

        $texture->save('texture.png');

        $canvas->insert($texture);

        $starField = StarField::generate($width, $height);

        if ($type->has_atmosphere) {
            $atmosphereImage = $this->generateAtmosphere($planetSize, $width, $height);
            $canvas->insert($atmosphereImage, 'center', $width / 2, $height / 2);
        }

        $starField->insert($canvas);

        return $starField;
    }

    private function generateAtmosphere(int $planetSize, int $width, int $height): \Intervention\Image\Image
    {
        $atmosphereImage = Image::canvas($width, $height);

        $holeImage = Image::canvas($width, $height, '#ffffff');
        $holeImage->circle($planetSize - 2, ($width / 2), ($height / 2), function ($draw) {
            $draw->background('#000000');
        });

        $atmosphereImage->circle($planetSize, ($width / 2) * 0.98, ($height / 2) * 0.98, function ($draw) {
            $draw->background('#ffffff');
        });

        $brightImage = Image::canvas($width, $height);
        $brightImage->circle($planetSize + 1, $width / 2, $height / 2, function ($draw) {
            $draw->background('#00aaff');
        });
        $brightImage->blur(4)->opacity(70)->mask($holeImage, false);

        $atmosphereImage->mask($holeImage, false)->opacity(70);
        $atmosphereImage->blur(10);
        $atmosphereImage->insert($brightImage);

        return $atmosphereImage;
    }

    private function generateBacklight(int $planetSize, int $width, int $height): \Intervention\Image\Image
    {
        $backlightImage = Image::canvas($width, $height);

        $circleMask = Image::canvas($width, $height);
        $circleMask->circle($planetSize, $width / 2, $height / 2, function ($draw) {
            $draw->background('#000000');
        });

        $holeImage = Image::canvas($width, $height, '#ffffff');
        $holeImage->circle($planetSize, ($width / 2) * 0.8, ($height / 2) * 0.8, function ($draw) {
            $draw->background('#000000');
        });

        $backlightImage->fill('#0000ff');
        $backlightImage->mask($holeImage, false);
        $backlightImage->blur(30);

        $backlightImage->mask($circleMask, true)->opacity(20);

        return $backlightImage;
    }

    private function generateClouds(int $planetSize, int $width, int $height, int $density): \Intervention\Image\Image
    {
        $cloudImage = Image::canvas($width, $height, '#ffffff');

        $circleMask = Image::canvas($width, $height);
        $circleMask->circle($planetSize, $width / 2, $height / 2, function ($draw) {
            $draw->background('#000000');
        });

        $noiseGen = new PerlinNoise($width, $height);
        $noiseMap = $noiseGen->generate();

        $img = new Imagick();
        $img->newImage($width, $height, 'rgb(0, 0, 0)');

        $iterator = $img->getPixelIterator();

        foreach ($iterator as $row => $pixels) {
            foreach ($pixels as $col => $pixel) {
                $h = 255 * $noiseMap[$row][$col];
                $threshold = 255 - $density;

                if ($h > $threshold) {
                    $color = "rgb($h, $h, $h)";
                } else {
                    $color = 'rgb(0, 0, 0)';
                }

                $pixel->setColor($color);
            }
            $iterator->syncIterator();
        }

        $cloudImage->mask($img, false)->blur(1);

        $cloudShadowImage = Image::canvas($width, $height);
        $cloudShadowImage->insert($cloudImage, 'center', $width / 2, $height / 2);
        $cloudShadowImage->brightness(-100)->opacity(70)->blur(2);

        $finalImage = Image::canvas($width, $height);
        $finalImage->insert($cloudShadowImage, 'center', ($width / 2) + 5, ($height / 2) + 5);
        $finalImage->insert($cloudImage, 'center', ($width / 2), ($height / 2));

        $finalImage->mask($circleMask, true);

        return $finalImage;
    }

    private function generateIceCaps(int $width, int $height): \Intervention\Image\Image
    {
        $north = $this->generateIceCap($width, $height);
        $south = $this->generateIceCap($width, $height);
        $south->flipImage();

        $image = Image::canvas($width, $height);
        $northernCaps = Image::make($north);
        $southernCaps = Image::make($south);
        $image->insert($northernCaps);
        $image->insert($southernCaps);

        return $image;
    }

    private function generateIceCap(int $width, int $height)
    {
        $pointSets = [];
        $startX = 0;
        $startY = $height / 8;

        $numberOfPoints = floor($width / 3);

        $stepX = floor($width / $numberOfPoints);

        $minStepY = -floor($height * 0.02);
        $maxStepY = floor($height * 0.02);

        $points = [['x' => $startX, 'y' => $startY]];

        $lastX = $startX;
        $baselineY = $startY;

        for ($i = 0; $i < $numberOfPoints; $i++) {
            $newX = $lastX + $stepX + mt_rand(-3, 3);

            $newY = $baselineY + mt_rand($minStepY, $maxStepY);
            $points [] = ['x' => $newX, 'y' => $newY];
            $lastX = $newX;

            if ($i > 3 && ($i % 4 == 0 || $i == $numberOfPoints)) {
                $pointSets [] = $points;
                $points = [['x' => $newX, 'y' => $newY]];
            }
        }

        $pointSets[sizeof($pointSets) - 1] [] = ['x' => $width, 'y' => $newY];

        $draw = new ImagickDraw();
        $strokeColor = new ImagickPixel('white');
        $fillColor = new ImagickPixel('rgba(255,255,255,0)');
        $draw->setStrokeOpacity(1);
        $draw->setStrokeColor($strokeColor);
        $draw->setFillColor($fillColor);
        $draw->setStrokeAntialias(false);
        $draw->setStrokeWidth(2);

        foreach ($pointSets as $points) {
            $draw->bezier($points);
        }

        $draw->line($width, $newY, $width, 0);
        $draw->line($width, 0, 0, 0);
        $draw->line(0, 0, 0, $startY);

        $imagick = new Imagick();
        $greenScreen = new ImagickPixel('#00FF08');
        $imagick->newImage($width, $height, $greenScreen);
        $imagick->drawImage($draw);

        $target = $imagick->getImagePixelColor(floor($width / 2), 10);
        $imagick->floodFillPaintImage('white', 10, $target, floor($width / 2), 10, false);
        $imagick->floodFillPaintImage(new ImagickPixel('rgba(0,0,0,0)'), 30, $greenScreen, 1, $height - 1, false, Imagick::CHANNEL_ALPHA);

        return $imagick;
    }

    private function generateShadow(int $planetSize, int $width, int $height): \Intervention\Image\Image
    {
        $shadowImage = Image::canvas($width, $height);

        $circleMask = Image::canvas($width, $height);
        $circleMask->circle($planetSize, $width / 2, $height / 2, function ($draw) {
            $draw->background('#000000');
        });

        $holeImage = Image::canvas($width, $height, '#ffffff');
        $holeImage->circle($planetSize, ($width / 2) * 0.7, ($height / 2) * 0.7, function ($draw) {
            $draw->background('#000000');
        });

        $shadowImage->fill('#000000');
        $shadowImage->mask($holeImage, false);
        $shadowImage->blur(30)->opacity(80);

        return $shadowImage;
    }
}
