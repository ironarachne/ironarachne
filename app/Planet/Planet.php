<?php


namespace App\Planet;


class Planet
{
    public string $name;
    public PlanetType $planet_type;
    public string $image_url;
    public string $size;

    public function __construct(string $name, PlanetType $type, string $url, string $size) {
        $this->name = $name;
        $this->image_url = $url;
        $this->planet_type = $type;
        $this->size = $size;
    }
}
