<?php


namespace App\Planet;


use App\Math\PerlinNoise;
use Imagick;
use Intervention\Image\Image;

class OceanPlanetType extends PlanetType
{
    public function __construct() {
        $this->name = 'ocean';
        $this->cloud_density = 100;
        $this->has_clouds = true;
        $this->has_polar_icecaps = false;
        $this->has_atmosphere = true;
        $this->size_small_min = 0.2;
        $this->size_small_max = 0.3;
        $this->size_medium_min = 0.3;
        $this->size_medium_max = 0.6;
        $this->size_large_min = 0.6;
        $this->size_large_max = 1.0;
    }

    public function getTexture(int $width = 512, int $height = 512): Image
    {
        $noiseGen = new PerlinNoise($width, $height);
        $elevation = $noiseGen->generate();

        $img = new Imagick();
        $img->newImage($width, $height, 'rgb(0,0,0)');

        $iterator = $img->getPixelIterator();

        foreach ($iterator as $row => $pixels) {
            foreach ($pixels as $col => $pixel) {
                $h = $elevation[$row][$col];

                if ($h > 100) {
                    $color = 'rgb(24, 37, 158)';
                } else {
                    $color = 'rgb(15, 28, 142)';
                }

                $pixel->setColor($color);
            }
            $iterator->syncIterator();
        }

        return \Intervention\Image\Facades\Image::make($img);
    }
}
