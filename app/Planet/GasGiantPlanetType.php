<?php


namespace App\Planet;


use App\Graphics\Color;
use App\Math\PerlinNoise;
use Imagick;
use Intervention\Image\Image;

class GasGiantPlanetType extends PlanetType
{
    public function __construct()
    {
        $this->name = 'gas giant';
        $this->cloud_density = 0;
        $this->has_atmosphere = false;
        $this->has_clouds = false;
        $this->has_polar_icecaps = false;
        $this->size_small_min = 0.7;
        $this->size_small_max = 0.8;
        $this->size_medium_min = 0.8;
        $this->size_medium_max = 0.9;
        $this->size_large_min = 0.9;
        $this->size_large_max = 1.0;
    }

    public function getTexture(int $width = 512, int $height = 512): Image
    {
        $noiseGen = new PerlinNoise($width, $height);
        $displacement = $noiseGen->generate();

        $color = Color::random();

        $img = new Imagick();
        $img->newImage($width, $height, 'rgb(0,0,0)');

        $colorLookup = [];

        for ($i=0;$i<$height;$i++) {
            $colorLookup[] = $color->toRGBString();
            $chanceOfChange = mt_rand(0, 100);
            if ($chanceOfChange > 70) {
                $color->r += mt_rand(-20, 20);
                $color->g += mt_rand(-20, 20);
                $color->b += mt_rand(-20, 20);
            }
        }

        $iterator = $img->getPixelIterator();

        foreach ($iterator as $row => $pixels) {
            foreach ($pixels as $col => $pixel) {
                $h = 10 * $displacement[$row][$col];
                if ($row + $h > $height) {
                    $c = $row;
                } else {
                    $c = $row + $h;
                }
                $pixel->setColor($colorLookup[$c]);
            }
            $iterator->syncIterator();
        }

        return \Intervention\Image\Facades\Image::make($img);
    }
}
