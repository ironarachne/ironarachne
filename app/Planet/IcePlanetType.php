<?php


namespace App\Planet;


use App\Math\PerlinNoise;
use Imagick;
use Intervention\Image\Facades\Image;

class IcePlanetType extends PlanetType
{
    public function __construct()
    {
        $this->name = 'ice';
        $this->cloud_density = 10;
        $this->has_atmosphere = true;
        $this->has_clouds = true;
        $this->has_polar_icecaps = false;
        $this->size_small_min = 0.2;
        $this->size_small_max = 0.3;
        $this->size_medium_min = 0.3;
        $this->size_medium_max = 0.6;
        $this->size_large_min = 0.6;
        $this->size_large_max = 1.0;
    }

    public function getTexture(int $width = 512, int $height = 512): \Intervention\Image\Image
    {
        $noiseGen = new PerlinNoise($width, $height);

        $elevation = $noiseGen->generate();
        $moisture = $noiseGen->generate();
        $temperature = $noiseGen->generate();

        $img = new Imagick();
        $img->newImage($width, $height, 'rgb(0,0,0)');

        $iterator = $img->getPixelIterator();

        foreach($iterator as $row => $pixels) {
            foreach($pixels as $col => $pixel) {
                $h = 255 * $elevation[$row][$col];
                $m = 255 * $moisture[$row][$col];
                $t = 255 * (($height - abs($row - ($height / 2))) / $height) + ($temperature[$row][$col] / 2);

                $color = $this->biome($h, $m, $t);

                $pixel->setColor($color);
            }
            $iterator->syncIterator();
        }

        return Image::make($img);
    }

    private function biome(float $elevation, float $moisture, float $temperature): string
    {
        if ($elevation < 50) {
            return 'rgb(173, 216, 213)'; // deep magma
        }

        if ($elevation < 75) {
            return 'rgb(195, 224, 222)'; // ocean
        }

        $r = 0;
        $g = 0;
        $b = 0;

        // adjust for moisture
        $moistureRatio = $moisture / 255;
        $r += (30 * $moistureRatio);
        $g += (30 * $moistureRatio);
        $b += (30 * $moistureRatio);

        // adjust for temperature
        $temperatureRatio = $temperature / 255;
        $r += (200 * $temperatureRatio);
        $b += (220 * $temperatureRatio);
        $g += (220 * $temperatureRatio);

        return "rgb($r, $g, $b)";
    }
}
