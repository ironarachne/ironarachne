<?php

namespace App\Http\Controllers;

use App\Heraldry;
use App\SavedCulture;
use App\SavedRegion;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Parsedown;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    public function admin()
    {
        Gate::authorize('admin-stats');

        $counts = [
            'heraldry' => Heraldry::count(),
            'region' => SavedRegion::count(),
            'culture' => SavedCulture::count(),
            'user' => User::count(),
        ];

        return view('admin.stats', ['counts' => $counts]);
    }

    public function index()
    {
        $posts = Cache::remember('blog_posts', 600, function () {
            $posts = [];

            $client = new Client(['base_uri' => 'https://blog.ironarachne.com/api/']);
            $response = $client->request('GET', 'collections/ben/posts');

            $body = (string)$response->getBody();

            $json = json_decode($body);

            $post_data = $json->data->posts;

            $Parsedown = new Parsedown();

            $i = 0;

            foreach ($post_data as $post) {
                if ($i < 3) {
                    $body = $Parsedown->text($post->body);
                    $body = str_replace('h2', 'h3', $body);

                    $posts[] = [
                        'title' => $post->title,
                        'created' => $post->created,
                        'body' => $body,
                    ];
                } else {
                    break;
                }

                $i++;
            }

            return $posts;
        });

        return view('index')->with(['posts' => $posts]);
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function dashboard()
    {
        $user = Auth::user();

        $cultures = $user->cultures()->latest()->get();
        $regions = $user->regions()->latest()->get();
        $devices = $user->heraldries()->latest()->get();

        return view('dashboard')->with(['cultures' => $cultures, 'regions' => $regions, 'devices' => $devices]);
    }

    public function about()
    {
        return view('about');
    }

    public function quick()
    {
        return view('quick');
    }

    public function privacy()
    {
        return view('privacy');
    }
}
