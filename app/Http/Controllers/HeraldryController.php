<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Heraldry\HeraldryGenerator;
use App\Heraldry;

class HeraldryController extends Controller
{
    public function index()
    {
        $devices = Heraldry::latest()->limit(15)->get();

        return view('heraldry.index', ['devices' => $devices]);
    }

    public function charges()
    {
        $charges = HeraldryGenerator::loadRasterCharges();

        return view('heraldry.charges', ['charges' => $charges]);
    }

    public function create(Request $request)
    {
        $guid = Uuid::uuid4();

        $fieldShape = $request->input('field_shape');
        $haveCharges = $request->input('have_charges');
        $chargeTag = $request->input('charge_tag');
        $division = $request->input('division');
        $variation1 = $request->input('variation_1');
        $variation2 = $request->input('variation_2');
        $primaryTincture = $request->input('primary_tincture');
        $bgTincture1 = $request->input('bg_tincture_1');
        $bgTincture2 = $request->input('bg_tincture_2');
        $bgTincture3 = $request->input('bg_tincture_3');
        $bgTincture4 = $request->input('bg_tincture_4');

        if (!empty($chargeTag)) {
            $preferredTags = [$chargeTag];
        } else {
            $preferredTags = [];
        }

        $heraldryGenerator = new HeraldryGenerator();
        $coa = $heraldryGenerator->generate($fieldShape, $preferredTags, [], $division, $primaryTincture, $bgTincture1, $bgTincture2, $bgTincture3, $bgTincture4, $haveCharges, true, $variation1, $variation2);

        $heraldry = new Heraldry();
        $heraldry->guid = $guid;
        $heraldry->blazon = $coa->blazon;
        $heraldry->url = $coa->image_url;

        if (Auth::check()) {
            $user = Auth::user();

            $user->heraldries()->save($heraldry);
        } else {
            $heraldry->save();
        }

        Cache::forever('heraldry-' . $heraldry->guid, $heraldry);

        return redirect()->route('heraldry.show', ['guid' => $heraldry->guid]);
    }

    public function show(Request $request, $guid)
    {
        $fieldShape = $request->query('shape');

        if (empty($fieldShape)) {
            $fieldShape = 'any';
        }

        $heraldry = Cache::rememberForever('heraldry-' . $guid, function () use ($fieldShape, $guid) {
            $h = Heraldry::where('guid', $guid)->first();

            if ($h == false) {
                $heraldryGenerator = new HeraldryGenerator();
                $coa = $heraldryGenerator->generate($fieldShape);

                $heraldry = new Heraldry();
                $heraldry->guid = $guid;
                $heraldry->blazon = $coa->blazon;
                $heraldry->url = $coa->image_url;

                if (Auth::check()) {
                    $user = Auth::user();

                    $user->heraldries()->save($heraldry);
                } else {
                    $heraldry->save();
                }
            } else {
                $heraldry = $h;
            }

            return $heraldry;
        });

        $heraldryGenerator = new HeraldryGenerator();
        $pennonURL = $heraldryGenerator->getPennonForDevice($heraldry->blazon);

        return view('heraldry.show', ['heraldry' => $heraldry, 'pennonURL' => $pennonURL]);
    }
}
