<?php

namespace App\Http\Controllers;

use App\Heraldry\HeraldryGenerator;
use App\NameGenerator\CommonFantasy;
use App\Organization\OrganizationGenerator;
use App\SavedOrganization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Ramsey\Uuid\Uuid;

class OrganizationController extends Controller
{
    public function index()
    {
        $organizations = SavedOrganization::latest()->limit(5)->get();

        return view('organization.index', ['organizations' => $organizations]);
    }

    public function create(Request $request)
    {
        $guid = Uuid::uuid4();

        $orgType = $request->input('type');

        $organizationGenerator = new OrganizationGenerator();
        $nameGenerator = new CommonFantasy();
        $organization = $organizationGenerator->generate($nameGenerator, $orgType);

        $heraldryGen = new HeraldryGenerator();
        $pennonURL = $heraldryGen->getPennonForDevice($organization->heraldry->blazon);

        $saved = new SavedOrganization();
        $saved->name = $organization->name;
        $saved->data = json_encode($organization);
        $saved->guid = $guid;
        $saved->description = $organization->name . ', ' . article($organization->type->name) . ' ' . $organization->type->name;
        $saved->html = view('organization.individual', ['organization' => $organization, 'pennonURL' => $pennonURL])->render();

        if (Auth::check()) {
            $user = Auth::user();

            $user->organizations()->save($saved);
        } else {
            $saved->save();
        }

        Cache::forever('organization-' . $saved->guid, $saved);

        return redirect()->route('organization.show', ['guid' => $saved->guid]);
    }

    public function show(string $guid)
    {
        $organization = Cache::rememberForever("organization-$guid", function () use ($guid) {
            $organization = SavedOrganization::where('guid', $guid)->first();
            if ($organization == false) {
                $organizationGenerator = new OrganizationGenerator();
                $nameGenerator = NameGenerator::defaultFantasy();
                $organization = $organizationGenerator->generate($nameGenerator);

                $heraldryGen = new HeraldryGenerator();
                $pennonURL = $heraldryGen->getPennonForDevice($organization->heraldry->blazon);

                $saved = new SavedOrganization();
                $saved->name = $organization->name;
                $saved->data = json_encode($organization);
                $saved->guid = $guid;
                $saved->description = $organization->name . ', ' . article($organization->type->name) . ' ' . $organization->type->name;
                $saved->html = view('organization.individual', ['organization' => $organization, 'pennonURL' => $pennonURL])->render();

                if (Auth::check()) {
                    $user = Auth::user();

                    $user->organizations()->save($saved);
                } else {
                    $saved->save();
                }

                return $saved;
            }

            return $organization;
        });

        return view('organization.show', ['organization' => $organization]);
    }
}
