<?php


namespace App\Http\Controllers;


use App\StarSystem\StarSystemGenerator;

class StarSystemController extends Controller
{
    public function index()
    {
        return view('star_system.index');
    }
}
