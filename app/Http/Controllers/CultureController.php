<?php

namespace App\Http\Controllers;

use App\BiomeGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Dompdf\Dompdf;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Cache;
use App\CultureGenerator;
use App\SavedCulture;

class CultureController extends Controller
{
    public function index()
    {
        $biomeOptions = [];
        $biomeGenerator = new BiomeGenerator();
        $biomes = $biomeGenerator->getBiomeData();

        foreach ($biomes as $biome) {
            if ($biome->type != 'marine') {
                $biomeOptions [] = $biome->name;
            }
        }

        sort($biomeOptions);

        $cultures = SavedCulture::latest()->limit(5)->get();

        return view('culture.index', ['cultures' => $cultures, 'biomeOptions' => $biomeOptions]);
    }

    public function create(Request $request)
    {
        $guid = Uuid::uuid4();

        $languageChoice = $request->input('language-choice');
        $biomeChoice = $request->input('biome');

        $cultureGenerator = new CultureGenerator();
        $culture = $cultureGenerator->generate($guid, $languageChoice, $biomeChoice);

        if (Auth::check()) {
            $user = Auth::user();

            $user->cultures()->save($culture);
        } else {
            $culture->save();
        }

        Cache::forever('culture-' . $culture->guid, $culture);

        return redirect()->route('culture.show', ['guid' => $culture->guid]);
    }

    public function pdf(string $guid)
    {
        $culture = Cache::rememberForever('culture-' . $guid, function () use ($guid) {
            $culture = SavedCulture::where('guid', '=', $guid)->first();
            return $culture;
        });

        $html = view('culture.pdf', ['culture' => $culture])->render();

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

        $dompdf->setPaper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream();
    }

    public function show(string $guid)
    {
        $culture = Cache::rememberForever('culture-' . $guid, function () use ($guid) {
            $culture = SavedCulture::where('guid', $guid)->first();
            if ($culture == false) {
                $cultureGenerator = new CultureGenerator();
                $culture = $cultureGenerator->generate($guid);

                if (Auth::check()) {
                    $user = Auth::user();

                    $user->cultures()->save($culture);
                } else {
                    $culture->save();
                }
            }
            return $culture;
        });

        return view('culture.show', ['culture' => $culture]);
    }
}
