<?php

namespace App\Http\Controllers;

use App\BiomeGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Cache;
use App\RegionGenerator;
use App\SavedRegion;

class RegionController extends Controller
{
    public function index()
    {
        $biomeOptions = [];
        $biomeGenerator = new BiomeGenerator();
        $biomes = $biomeGenerator->getBiomeData();

        foreach ($biomes as $biome) {
            if ($biome->type != 'marine') {
                $biomeOptions [] = $biome->name;
            }
        }

        sort($biomeOptions);

        $regions = SavedRegion::latest()->limit(5)->get();

        return view('region.index', ['regions' => $regions, 'biomes' => $biomeOptions]);
    }

    public function create(Request $request)
    {
        $guid = Uuid::uuid4();

        $languageChoice = $request->input('language-choice');
        $biomeChoice = $request->input('biome');

        $regionGenerator = new RegionGenerator();
        $region = $regionGenerator->generate($guid, $languageChoice, $biomeChoice);

        if (Auth::check()) {
            $user = Auth::user();

            $user->regions()->save($region);
        } else {
            $region->save();
        }

        Cache::forever('region-' . $region->guid, $region);

        return redirect()->route('region.show', ['guid' => $region->guid]);
    }

    public function show($guid)
    {
        $region = Cache::rememberForever('region-' . $guid, function () use ($guid) {
            $region = SavedRegion::where('guid', $guid)->first();
            if ($region == false) {
                $regionGenerator = new RegionGenerator();

                $region = $regionGenerator->generate($guid);

                if (Auth::check()) {
                    $user = Auth::user();

                    $user->regions()->save($region);
                } else {
                    $region->save();
                }
            }
            return $region;
        });

        return view('region.show', ['region' => $region]);
    }
}
