<?php


namespace App\Organization;

use App\Goal;
use App\LeadershipType;
use App\Profession;
use App\Title\Title;

class HolyOrder extends OrganizationType
{
    public function __construct()
    {
        $this->name = 'holy order';
        $this->max_size = 1000;
        $this->min_size = 10;

        $wealthGoal = new Goal();
        $wealthGoal->name = 'amass wealth';
        $wealthGoal->description_template = '{{Name}} seeks to amass wealth';
        $wealthGoal->compatible_traits = ['avaricious', 'greedy'];
        $wealthGoal->incompatible_traits = ['generous', 'charitable'];

        $salvationGoal = new Goal();
        $salvationGoal->name = 'salvation';
        $salvationGoal->description_template = '{{Name}} seeks to save the souls of the people';
        $salvationGoal->compatible_traits = ['zealous', 'pious', 'devout'];
        $salvationGoal->incompatible_traits = ['impious', 'materialistic'];

        $conversionGoal = new Goal();
        $conversionGoal->name = 'conversion';
        $conversionGoal->description_template = '{{Name}} strives to convert everyone to {{PossessivePronoun}} faith';
        $conversionGoal->compatible_traits = ['devout', 'pious'];
        $conversionGoal->incompatible_traits = ['impious', 'reserved'];

        $sinGoal = new Goal();
        $sinGoal->name = 'eliminate sin';
        $sinGoal->description_template = '{{Name}} is driven to root out and eliminate sin';
        $sinGoal->compatible_traits = ['devout', 'pious', 'fanatical', 'controlling', 'zealous', 'ruthless'];
        $sinGoal->incompatible_traits = ['helpful', 'honorable', 'reserved'];

        $this->possible_goals = [$wealthGoal, $salvationGoal, $conversionGoal, $sinGoal];

        $this->possible_heraldry_charge_tags = [
            'church',
            'armor',
            'crescent',
            'heavens',
            'passive',
        ];
        $this->member_professions = Profession::load('divine');

        $highPriestTitle = new Title('High Priest', 'High Priestess', '', '', 'holy order', 2);
        $priestTitle = new Title('Priest', 'Priestess', '', '', 'holy order', 1);
        $acolyteTitle = new Title('Acolyte', 'Acolyte', '', '', 'holy order', 0);

        $this->leadership_type = new LeadershipType('priesthood', 1, 4, $highPriestTitle);

        $this->ranks = [
            new OrganizationRank('High Priest', $highPriestTitle, 4, ['adult', 'elderly'], true),
            new OrganizationRank('Priest', $priestTitle, 0),
            new OrganizationRank('Acolyte', $acolyteTitle, 0),
        ];

    }

    public function generateNames(): array
    {
        $prefixes = [
            'Holy',
            'Glorious',
            'Exalted',
            'Humble',
            'Penitent',
            'Righteous',
        ];

        $groupTypes = [
            'Church',
            'Order',
        ];

        $suffixes = [
            'Divine Hand',
            'Dove',
            'Eye',
            'Flame',
            'Forest',
            'Four Truths',
            'Iron Path',
            'Light',
            'Meek',
            'Noble Path',
            'Noble Truth',
            'Path',
            'Winding River',
            'Sword',
            'Three Truths',
            'Truth',
            'Unbroken Sword',
        ];

        $prefix = random_item($prefixes);
        $type = random_item($groupTypes);
        $suffix = random_item($suffixes);

        return ["the $prefix $type of the $suffix", "the $type"];
    }
}
