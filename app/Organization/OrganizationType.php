<?php


namespace App\Organization;

use App\LeadershipType;

abstract class OrganizationType
{
    public string $name;
    public int $min_size;
    public int $max_size;
    public array $possible_goals;
    public bool $has_heraldry;
    public array $possible_heraldry_charge_tags;
    public LeadershipType $leadership_type;
    public array $ranks;
    public array $member_professions;

    public abstract function generateNames();
}
