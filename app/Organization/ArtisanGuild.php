<?php


namespace App\Organization;

use App\Goal;
use App\LeadershipType;
use App\Profession;
use App\Title\Title;
use Illuminate\Support\Str;

class ArtisanGuild extends OrganizationType
{
    public Profession $profession;

    public function __construct(Profession $profession)
    {
        $this->name = 'artisan guild';
        $this->max_size = 150;
        $this->min_size = 10;

        $wealthGoal = new Goal();
        $wealthGoal->name = 'amass wealth';
        $wealthGoal->description_template = '{{Name}} seeks to amass wealth';
        $wealthGoal->compatible_traits = ['avaricious', 'greedy'];
        $wealthGoal->incompatible_traits = ['generous', 'charitable'];

        $reputationGoal = new Goal();
        $reputationGoal->name = 'craftsmanship reputation';
        $reputationGoal->description_template = '{{Name}} wants to reinforce {{PossessivePronoun}} reputation for craftsmanship';
        $reputationGoal->compatible_traits = ['skilled', 'proud', 'arrogant'];
        $reputationGoal->incompatible_traits = ['lazy', 'unskilled', 'meek', 'humble'];

        $skillGoal = new Goal();
        $skillGoal->name = 'skill';
        $skillGoal->description_template = '{{Name}} wants to be known for their skill and craftsmanship';
        $skillGoal->compatible_traits = ['arrogant', 'proud', 'ambitious'];
        $skillGoal->incompatible_traits = ['lazy', 'unskilled'];

        $this->possible_goals = [$wealthGoal, $reputationGoal, $skillGoal];

        $this->possible_heraldry_charge_tags = [
            'basket',
            'hand',
            'garb',
            'object',
            'herb',
            'plant',
            'tool',
        ];
        $this->profession = $profession;
        $this->member_professions = [$profession];

        $leaderTitle = new Title('Guild Leader', 'Guild Leader', '', '', 'artisan guild', 2);
        $artisanTitle = new Title('Artisan', 'Artisan', '', '', 'artisan guild', 1);
        $apprenticeTitle = new Title('Apprentice', 'Apprentice', '', '', 'artisan guild', 0);

        $this->leadership_type = new LeadershipType('council', 2, 4, $leaderTitle);
        $this->ranks = [
            new OrganizationRank('Guild Leader', $leaderTitle, 4, ['adult', 'elderly'], true),
            new OrganizationRank('Artisan', $artisanTitle, 0),
            new OrganizationRank('Apprentice Artisan', $apprenticeTitle, 0, ['teenager', 'young adult']),
        ];
    }

    public function generateNames(): array
    {
        $qualifiers = [
            'August',
            'East Wind',
            'Global',
            'Imperial',
            'Incorporated',
            'North Wind',
            'Royal',
            'South Wind',
            'West Wind',
        ];

        $qualifier = random_item($qualifiers);

        $professionName = ucfirst($this->profession->name);

        $professionNamePlural = Str::plural($professionName);

        $patterns = [
            "$qualifier Guild of {$professionNamePlural}",
            "$qualifier $professionName's Guild",
        ];

        return ['the ' . random_item($patterns), 'the Guild'];
    }
}
