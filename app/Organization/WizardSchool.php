<?php


namespace App\Organization;

use App\Character;
use App\Goal;
use App\Profession;
use App\LeadershipType;
use App\Title\Title;

class WizardSchool extends OrganizationType
{
    public Character $founder;

    public function __construct(Character $founder)
    {
        $this->name = 'wizard school';
        $this->max_size = 500;
        $this->min_size = 50;

        $secretsGoal = new Goal();
        $secretsGoal->name = 'keep secrets';
        $secretsGoal->description_template = '{{Name}} hides arcane secrets to prevent catastrophe';
        $secretsGoal->compatible_traits = ['careful', 'wise'];
        $secretsGoal->incompatible_traits = ['reckless'];

        $instructionGoal = new Goal();
        $instructionGoal->name = 'teach magic';
        $instructionGoal->description_template = '{{Name}} desires to teach {{PossessivePronoun}} type of magic to others';
        $instructionGoal->compatible_traits = ['knowledgeable', 'ambitious'];
        $instructionGoal->incompatible_traits = ['aloof', 'closeted'];

        $learnGoal = new Goal();
        $learnGoal->name = 'learn magic';
        $learnGoal->description_template = '{{Name}} seeks to study magic and learn new secrets';
        $learnGoal->compatible_traits = ['curious', 'patient', 'wise'];
        $learnGoal->incompatible_traits = ['lazy'];

        $this->possible_goals = [$secretsGoal, $instructionGoal, $learnGoal];

        $this->possible_heraldry_charge_tags = [
            'animal',
            'castle',
            'monster',
        ];
        $this->founder = $founder;
        $this->member_professions = Profession::load('wizard');

        $headmasterTitle = new Title('Headmaster', 'Headmaster', '', '', 'wizard school', 3);
        $instructorTitle = new Title('Instructor', 'Instructor', '', '', 'wizard school', 2);
        $groundsKeeperTitle = new Title('Groundskeeper', 'Groundskeeper', '', '', 'wizard school', 1);
        $studentTitle = new Title('Student', 'Student', '', '', 'wizard school', 0);

        $this->leadership_type = new LeadershipType('headmastership', 1, 1, $headmasterTitle);
        $this->ranks = [
            new OrganizationRank('Headmaster', $headmasterTitle, 1, ['adult', 'elderly'], true),
            new OrganizationRank('Instructor', $instructorTitle, 0),
            new OrganizationRank('Groundskeeper', $groundsKeeperTitle, 1),
            new OrganizationRank('Student', $studentTitle, 0, ['child', 'teenager']),
        ];
    }

    public function generateNames(): array
    {
        $schools = [
            'School',
            'Academy',
            'Institute',
            'College',
        ];

        $school = random_item($schools);

        $qualifiers = [
            'Wizardry',
            'Arcane Sciences',
            'Arcane Studies',
            'Arcane Matters',
            'Arcane Endeavours',
            'Eldritch Sciences',
            'Eldritch Studies',
            'Wizarding Arts',
            'Wizardry and Magic',
            'Sorcerous Endeavours',
            'Mystical Arts',
            'Mystical Studies',
        ];

        $qualifier = random_item($qualifiers);

        $conjunction = random_item(['for', 'of']);

        $patterns = [
            "{$this->founder->last_name}'s $school $conjunction $qualifier",
            "{$this->founder->last_name} $school $conjunction $qualifier",
            "the $school $conjunction $qualifier",
        ];

        $pattern = random_item($patterns);

        if (substr($pattern, 0, 3) == 'the') {
            $shortName = "the $school";
        } elseif (str_contains($pattern, "'s")) {
            $shortName = $this->founder->last_name . "'s";
        } else {
            $shortName = $this->founder->last_name . " $school";
        }

        return [$pattern, $shortName];
    }
}
