<?php


namespace App\Organization;


use App\Character;
use App\Goal;

class Faction
{
    public Character $leader;
    public string $support;
    public string $description;
    public Goal $goal;

    public function describe(string $organizationName, string $organizationShortName): string
    {
        $term = random_item([$organizationName, $organizationShortName, 'this organization']);

        $description = $this->leader->getFullName() . " leads a faction within $term. ";
        $description .= $this->goal->describe($this->leader->first_name, $this->leader->gender) . ', and ';
        $description .= pronoun($this->leader->gender, 'subjective') . ' has ' . $this->support . '.';

        return $description;
    }
}
