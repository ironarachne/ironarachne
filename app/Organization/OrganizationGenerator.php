<?php


namespace App\Organization;


use App\Character;
use App\CharacterGenerator;
use App\Goal;
use App\Heraldry\CoatOfArms;
use App\Heraldry\HeraldryGenerator;
use App\NameGenerator\CommonFantasy;
use App\NameGenerator\NameGenerator;
use App\PersonalityTrait;
use App\Profession;
use App\Species;
use App\Title\Title;
use Exception;
use Illuminate\Support\Str;

class OrganizationGenerator
{
    public function generate(NameGenerator $nameGenerator, string $type = 'random')
    {
        $organization = new Organization();

        $organization->type = $this->getOrganizationType($type);

        $organization->negative_traits = [];
        $organization->positive_traits = [];
        $negativeTraits = PersonalityTrait::randomNegative();
        $positiveTraits = PersonalityTrait::randomPositive();
        $positiveTraits = PersonalityTrait::removeIncompatible($negativeTraits, $positiveTraits);
        foreach ($negativeTraits as $t) {
            $organization->negative_traits [] = $t->name;
        }
        foreach ($positiveTraits as $t) {
            $organization->positive_traits [] = $t->name;
        }

        $organization->primary_goal = Goal::random($organization->type->possible_goals);

        $organization->number_of_members = mt_rand($organization->type->min_size, $organization->type->max_size);

        $organization->size = $organization->getSizeClass();

        $names = $organization->type->generateNames();
        $organization->name = $names[0];
        $organization->short_name = $names[1];

        $organization->heraldry = $this->generateHeraldry($organization->name, $organization->type->possible_heraldry_charge_tags);

        $organization->leaders = [];
        $organization->leaders = $this->generateLeaders($nameGenerator, $organization);

        $organization->factions = $this->generateFactions($organization->type->possible_goals, $organization->number_of_members, $nameGenerator, $organization);

        $organization->notable_members = [];
        $organization->notable_members = $this->generateNotableMembers($nameGenerator, $organization);

        if (sizeof($organization->factions) > 0) {
            foreach ($organization->factions as $faction) {
                $organization->notable_members [] = $faction->leader;
            }
        }

        $organization->description = $organization->describe() . ' ' . $this->randomAdditionalTrait($organization);

        return $organization;
    }

    private function generateFactions(array $possibleGoals, int $numberOfOrganizationMembers, NameGenerator $nameGen, Organization $organization): array
    {
        if ($numberOfOrganizationMembers < 20) {
            return [];
        }

        $factions = [];

        $possibleNumberOfFactions = floor($numberOfOrganizationMembers / 20);
        if ($possibleNumberOfFactions > 5) {
            $possibleNumberOfFactions = 5;
        }

        $numberOfFactions = mt_rand(1, $possibleNumberOfFactions);

        $supportLevels = [
            'few followers',
            'strong support',
            'growing support',
            'a lot of followers',
            'many followers',
            'some support',
            'waning support',
        ];

        for ($i = 0; $i < $numberOfFactions; $i++) {
            $faction = new Faction();
            $faction->goal = Goal::random($possibleGoals);
            $faction->support = random_item($supportLevels);
            $leader = $this->randomMember($nameGen, $organization);
            $faction->leader = $leader;
            $faction->description = $faction->describe($organization->name, $organization->short_name);

            $factions [] = $faction;
        }

        return $factions;
    }

    private function generateHeraldry(string $name, array $tags): CoatOfArms
    {
        $primaryTincture = 'any';
        $gen = new HeraldryGenerator();

        $preferredTags = [];

        $nameComponents = explode(' ', strtolower($name));

        foreach ($nameComponents as $c) {
            if (in_array($c, ['a', 'the', 'of'])) {
                continue;
            }

            if (Str::endsWith($c, "'s")) {
                $c = substr($c, 0, -2);
            }

            if (in_array($c, ['iron', 'white', 'silver'])) {
                $primaryTincture = 'argent';
            } elseif (in_array($c, ['golden', 'yellow', 'gilded', 'gold'])) {
                $primaryTincture = 'Or';
            } elseif (in_array($c, ['burning', 'blood', 'red', 'crimson'])) {
                $primaryTincture = 'gules';
            } elseif (in_array($c, ['black'])) {
                $primaryTincture = 'sable';
            }

            $c = Str::singular($c);

            $preferredTags [] = $c;
        }

        return $gen->generate('any', $preferredTags, $tags, 'any', $primaryTincture);
    }

    private function generateLeaders(NameGenerator $nameGenerator, Organization $organization): array
    {
        $leaders = [];

        $leaderTitle = $organization->type->leadership_type->title;
        $leaderAgeCategories = $organization->getLeaderAgeCategories();
        $minLeaders = $organization->type->leadership_type->min_leaders;
        $maxLeaders = $organization->type->leadership_type->max_leaders;
        $numberOfLeaders = mt_rand($minLeaders, $maxLeaders);

        $charGen = new CharacterGenerator();
        $species = Species::randomRace();

        for ($i = 0; $i < $numberOfLeaders; $i++) {
            $leader = $charGen->generate($nameGenerator, $species, $leaderAgeCategories);
            $leader->profession = $organization->randomProfession();
            $leader->primary_title = $leaderTitle->getPrefix($leader->gender);
            $leader->titles = [$leaderTitle];

            $leader->description = $leader->describe();
            $leaders [] = $leader;
        }

        return $leaders;
    }

    private function generateNotableMembers(NameGenerator $nameGenerator, Organization $organization): array
    {
        $members = [];

        $numberOfMembers = mt_rand(1, 4);

        for ($i = 0; $i < $numberOfMembers; $i++) {
            $member = $this->randomMember($nameGenerator, $organization);
            $members [] = $member;
        }

        return $members;
    }

    private function getOrganizationType(string $name): OrganizationType {
        if ($name == 'random') {
            return $this->randomType();
        } elseif ($name == 'adventuring company') {
            return new AdventuringCompany();
        } elseif ($name == 'artisan guild') {
            $profession = random_item(Profession::load('crafter'));
            return new ArtisanGuild($profession);
        } elseif ($name == 'holy order') {
            return new HolyOrder();
        } elseif ($name == 'mercenary company') {
            return new MercenaryCompany();
        } elseif ($name == 'wizard school') {
            $charGen = new CharacterGenerator();
            $nameGenerator = new CommonFantasy();
            $species = random_item(Species::load('race'));
            $founder = $charGen->generate($nameGenerator, $species, ['elderly']);
            $founder->profession = Profession::randomByTag('wizard');
            $founderTitle = new Title('Archmage', 'Archmage', '', '', 'wizard school', 5);
            $founder->titles [] = $founderTitle;
            if ($founder->primary_title == '') {
                $founder->primary_title = $founderTitle->getPrefix($founder->gender);
            }
            return new WizardSchool($founder);
        } else {
            throw new Exception('tried to get nonexistent organization type');
        }
    }

    private function randomAdditionalTrait(Organization $organization): string
    {
        $possibleTraits = [
            '{{Name}} is well-funded, though no one seems to know where that money comes from.',
            '{{ShortName}} is well-funded, and there are rumors that a major noble is bankrolling them.',
            'Unlike other {{TypePlural}}, {{Name}} has strong influence in the area.',
            'Curiously, {{ShortName}} seems uninterested in local affairs.',
            '{{ShortName}} enjoys surprising amounts of local popularity.',
            '{{ShortName}}\'s members often gather at a specific local tavern to relax.',
            'The kind of local popularity that {{ShortName}} enjoys is impressive.',
            'Locally distrusted, {{ShortName}} nonetheless enjoys a good reputation further afield.',
        ];

        $trait = random_item($possibleTraits);
        $trait = str_replace('{{Name}}', $organization->name, $trait);
        $trait = str_replace('{{ShortName}}', $organization->short_name, $trait);
        $trait = str_replace('{{Type}}', $organization->type->name, $trait);
        $trait = str_replace('{{TypePlural}}', Str::plural($organization->type->name), $trait);

        return ucfirst($trait);
    }

    private function randomMember(NameGenerator $nameGenerator, Organization $organization): Character
    {
        $charGen = new CharacterGenerator();
        $species = Species::randomRace();
        $rank = $organization->getRandomMemberRank();
        $member = $charGen->generate($nameGenerator, $species, $rank->possible_age_categories);
        $member->profession = $organization->randomProfession();
        $title = $rank->title;
        $member->titles = [$title];
        $member->primary_title = $title->getPrefix($member->gender);
        $member->description = $member->describe();

        return $member;
    }

    private function randomType(): OrganizationType
    {
        $types = [
            'adventuring company',
            'artisan guild',
            'holy order',
            'mercenary company',
            'wizard school',
        ];

        $type = random_item($types);

        return $this->getOrganizationType($type);
    }
}
