<?php


namespace App\Organization;


use App\Goal;
use App\Heraldry\CoatOfArms;
use App\Profession;

class Organization
{
    public string $name;
    public string $short_name;
    public string $size;
    public int $number_of_members;
    public string $description;
    public array $notable_members;
    public array $leaders;
    public array $positive_traits;
    public array $negative_traits;
    public array $factions;
    public Goal $primary_goal;
    public CoatOfArms $heraldry;
    public OrganizationType $type;

    public function __construct()
    {
        $this->notable_members = [];
        $this->leaders = [];
        $this->positive_traits = [];
        $this->negative_traits = [];
    }

    public function describe(): string
    {
        $leaderNames = [];

        foreach ($this->leaders as $leader) {
            $leaderNames [] = $leader->primary_title . " {$leader->first_name} {$leader->last_name}";
        }

        $traits = "This {$this->type->name} is known for being " . combine_phrases($this->positive_traits);
        $traits .= ", but also " . combine_phrases($this->negative_traits) . '.';

        $description = ucfirst($this->name) . " is " . article($this->size) . " {$this->size} {$this->type->name} with ";
        $description .= "{$this->number_of_members} members. It's led by " . combine_phrases($leaderNames) . '. ';
        $description .= ucfirst($this->primary_goal->describe($this->short_name, 'neutral')) . '. ';
        $description .= "$traits Its heraldry can be described as '{$this->heraldry->blazon}.'";

        return $description;
    }

    public function getLeaderAgeCategories(): array
    {
        $leaderRankName = $this->type->leadership_type->title->male_prefix;

        foreach ($this->type->ranks as $rank) {
            if ($rank->title->male_prefix == $leaderRankName) {
                return $rank->possible_age_categories;
            }
        }

        return [];
    }

    public function getMembersOfRank(OrganizationRank $rank): array
    {
        $result = [];

        foreach ($this->notable_members as $member) {
            foreach ($member->titles as $title) {
                if ($title->male_prefix == $rank->title->male_prefix) {
                    $result [] = $member;
                }
            }
        }

        return $result;
    }

    public function getRandomMemberRank(): OrganizationRank
    {
        $possibleRanks = [];

        foreach ($this->type->ranks as $rank) {
            if ($rank->is_leader) {
                continue;
            }

            $membersOfRank = $this->getMembersOfRank($rank);

            if ($rank->maximum_holders == 0 || sizeof($membersOfRank) < $rank->maximum_holders) {
                $possibleRanks [] = $rank;
            }
        }

        $rank = random_item($possibleRanks);

        return $rank;
    }

    public function getSizeClass(): string
    {
        $min = $this->type->min_size;
        $max = $this->type->max_size;
        $number = $this->number_of_members;
        $range = $max - $min;

        if ($number < ($min + ($range / 3))) {
            return 'small';
        } else if ($number < ($min + ($range / 2))) {
            return 'medium';
        } else if ($number < ($min + (($range * 2) / 3))) {
            return 'large';
        }

        return 'huge';
    }

    public function randomProfession(): Profession
    {
        $professions = $this->type->member_professions;

        return random_item($professions);
    }
}
