<?php


namespace App\Organization;

use App\Goal;
use App\Profession;
use App\LeadershipType;
use App\Title\Title;

class MercenaryCompany extends OrganizationType
{
    public function __construct()
    {
        $this->name = 'mercenary company';
        $this->max_size = 500;
        $this->min_size = 20;

        $wealthGoal = new Goal();
        $wealthGoal->name = 'amass wealth';
        $wealthGoal->description_template = '{{Name}} seeks to amass wealth';
        $wealthGoal->compatible_traits = ['avaricious', 'greedy'];
        $wealthGoal->incompatible_traits = ['generous', 'charitable'];

        $powerGoal = new Goal();
        $powerGoal->name = 'gain power';
        $powerGoal->description_template = '{{Name}} desires power';
        $powerGoal->compatible_traits = ['ambitious'];
        $powerGoal->incompatible_traits = ['selfless'];

        $protectGoal = new Goal();
        $protectGoal->name = 'protect people';
        $protectGoal->description_template = '{{Name}} strives to protect people';
        $protectGoal->compatible_traits = ['selfless', 'heroic'];
        $protectGoal->incompatible_traits = ['selfish', 'cowardly'];

        $this->possible_goals = [$wealthGoal, $powerGoal, $protectGoal];

        $this->possible_heraldry_charge_tags = [
            'aggressive',
            'armor',
            'monster',
            'weapon',
        ];
        $this->member_professions = Profession::load('fighter');

        $captainTitle = new Title('Captain', 'Captain', '', '', 'mercenary company', 2);
        $sergeantTitle = new Title('Sergeant', 'Sergeant', '', '', 'mercenary company', 1);
        $mercenaryTitle = new Title('Mercenary', 'Mercenary', '', '', 'mercenary company', 0);

        $this->leadership_type = new LeadershipType('captaincy', 1, 1, $captainTitle);
        $this->ranks = [
            new OrganizationRank('Captain', $captainTitle, 1, ['adult'], true),
            new OrganizationRank('Sergeant', $sergeantTitle, 10, ['adult']),
            new OrganizationRank('Mercenary', $mercenaryTitle, 0),
        ];
    }

    public function generateNames(): array
    {
        $prefixes = [
            'Black',
            'Blood',
            'Burning',
            'Crimson',
            'Free',
            'Gilded',
            'Golden',
            'Iron',
            'Red',
            'Silver',
            'White',
        ];

        $suffixes = [
            'Axes',
            'Army',
            'Bears',
            'Blades',
            'Coins',
            'Company',
            'Dragons',
            'Giants',
            'Lords',
            'Pikes',
            'Sentinels',
            'Swords',
            'Wolves',
            'Wyverns',
        ];

        $prefix = random_item($prefixes);
        $suffix = random_item($suffixes);

        return ["the $prefix $suffix", "the $suffix"];
    }
}
