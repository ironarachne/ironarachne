<?php


namespace App\Organization;


use App\Goal;
use App\LeadershipType;
use App\Profession;
use App\Title\Title;

class AdventuringCompany extends OrganizationType
{
    public function __construct()
    {
        $this->name = 'adventuring company';
        $this->max_size = 50;
        $this->min_size = 2;

        $wealthGoal = new Goal();
        $wealthGoal->name = 'amass wealth';
        $wealthGoal->description_template = '{{Name}} seeks to amass wealth';
        $wealthGoal->compatible_traits = ['avaricious', 'greedy'];
        $wealthGoal->incompatible_traits = ['generous', 'charitable'];

        $powerGoal = new Goal();
        $powerGoal->name = 'gain power';
        $powerGoal->description_template = '{{Name}} desires power';
        $powerGoal->compatible_traits = ['ambitious'];
        $powerGoal->incompatible_traits = ['selfless'];

        $protectGoal = new Goal();
        $protectGoal->name = 'protect people';
        $protectGoal->description_template = '{{Name}} strives to protect people';
        $protectGoal->compatible_traits = ['selfless', 'heroic'];
        $protectGoal->incompatible_traits = ['selfish', 'cowardly'];

        $this->possible_goals = [$wealthGoal, $powerGoal, $protectGoal];

        $this->possible_heraldry_charge_tags = [
            'aggressive',
            'armor',
            'monster',
            'weapon',
        ];

        $professions = Profession::load('divine');
        $professions = array_merge($professions, Profession::load('fighter'));
        $professions = array_merge($professions, Profession::load('mage'));

        $this->member_professions = $professions;

        $captainTitle = new Title('Captain', 'Captain', '', '', 'adventuring company', 1);
        $adventurerTitle = new Title('Adventurer', 'Adventurer', '', '', 'adventuring company', 0);

        $this->leadership_type = new LeadershipType('captaincy', 1, 1, $captainTitle);
        $this->ranks = [
            new OrganizationRank('Captain', $captainTitle, 1, ['adult'], true),
            new OrganizationRank('Adventurer', $adventurerTitle, 0, ['young adult', 'adult'], false),
        ];
    }

    public function generateNames(): array
    {
        $prefixes = [
            'Black',
            'Burning',
            'Crimson',
            'Free',
            'Golden',
            'Iron',
            'Righteous',
            'Silver',
            'Wandering',
            'White',
        ];

        $suffixes = [
            'Axes',
            'Blades',
            'Bears',
            'Coins',
            'Company',
            'Dragons',
            'Flame',
            'Foxes',
            'Giants',
            'Lords',
            'Pikes',
            'Swords',
            'Wolves',
            'Wyverns',
        ];

        $prefix = random_item($prefixes);
        $suffix = random_item($suffixes);

        return ["the $prefix $suffix", "the $suffix"];
    }
}
