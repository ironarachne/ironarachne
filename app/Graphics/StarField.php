<?php


namespace App\Graphics;


use Intervention\Image\Facades\Image;

class StarField
{
    public static function generate(int $width, int $height): \Intervention\Image\Image
    {
        $canvas = Image::canvas($width, $height, '#000000');

        $numberOfStars = $width * $height * 0.003;

        $colors = [
            [155, 155, 155],
            [137, 152, 150],
            [54, 75, 72],
            [87, 93, 92],
        ];

        for ($i = 0; $i < $numberOfStars; $i++) {
            $x = mt_rand(1, $width);
            $y = mt_rand(1, $height);

            $color = random_item($colors);
            $canvas->pixel($color, $x, $y);
        }

        return $canvas;
    }
}
