<?php


namespace App\Graphics;


class Color
{
    public int $r;
    public int $g;
    public int $b;

    public function __construct(int $r, int $g, int $b)
    {
        $this->r = $r;
        $this->g = $g;
        $this->b = $b;
    }

    public function toRGBString(): string
    {
        return 'rgb(' . $this->r . ', ' . $this->g . ', ' . $this->b . ')';
    }

    public static function random(): Color
    {
        $r = mt_rand(0, 255);
        $g = mt_rand(0, 255);
        $b = mt_rand(0, 255);

        return new Color($r, $g, $b);
    }
}
