<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heraldry extends Model
{
    public function creator()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'claimant_id');
    }
}
