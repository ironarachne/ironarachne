<?php

namespace App;

use App\NameGenerator\NameGenerator;

class CultureGenerator
{
    public function generate(string $id, string $languageType = 'any', string $biomeType = 'any'): SavedCulture
    {
        $geoGen = new GeographicRegionGenerator();
        $geography = $geoGen->generate($biomeType);
        $resources = $geography->resources();

        $langGen = new LanguageGenerator();
        if ($languageType == 'common') {
            $language = $langGen->getCommon();
        } elseif ($languageType == 'conlang') {
            $language = $langGen->generate();
        } else {
            $language = $langGen->random();
        }

        $nameGenerator = new NameGenerator();
        $nameGenerator->female_first_names = $language->female_first_names;
        $nameGenerator->female_last_names = $language->female_last_names;
        $nameGenerator->male_first_names = $language->male_first_names;
        $nameGenerator->male_last_names = $language->male_last_names;
        $nameGenerator->place_names = $language->place_names;
        $nameGenerator->culture_names = $language->culture_names;

        $name = $nameGenerator->randomCultureName();
        $adjective = $name;

        $names['male'] = $nameGenerator->randomSetOfNames('male first', 10);
        $names['female'] = $nameGenerator->randomSetOfNames('female first', 10);
        $names['family'] = $nameGenerator->randomSetOfNames('female last', 10);

        $musicGen = new MusicGenerator();
        $music = $musicGen->generate($id);

        $clothingGen = new ClothingStyleGenerator();
        $clothing = $clothingGen->generate();

        $drinkGen = new AlcoholicDrinkGenerator();
        $drink = $drinkGen->generate($resources, strtolower($language->translate('drink')));

        $cuisineGen = new CuisineGenerator();
        $cuisine = $cuisineGen->generate($resources);

        $religionGen = new ReligionGenerator();
        $religion = $religionGen->generate($nameGenerator);

        $cultureData = new Culture($name, $adjective, $language, $geography, $music, $clothing, $cuisine, $drink, $religion);

        $culture = new SavedCulture();
        $culture->data = json_encode($cultureData);
        $culture->guid = $id;

        $culture->name = $cultureData->name;
        $culture->description = 'The ' . $cultureData->name . ' people, a fictional culture from a ' . $cultureData->geography->biome->name . ' climate.';
        $culture->html = view('culture.individual', ['culture' => $cultureData, 'names' => $names])->render();

        return $culture;
    }
}
