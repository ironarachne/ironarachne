<?php


namespace App\Heraldry\Variation;


use Intervention\Image\Facades\Image;

class BarryVariation extends Variation
{
    public function __construct()
    {
        $this->name = 'barry';
        $this->number_of_tinctures = 2;
        $this->blazon = '';
        $this->commonality = 5;
        $this->allow_furs = false;
        $this->tinctures = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $img = Image::canvas($width, $height);

        $primaryTincture = $this->tinctures[0]->type == 'fur' ? resource_path() . '/img/patterns/' . $this->tinctures[0]->pattern_file_name : $this->tinctures[0]->color;
        $secondaryTincture = $this->tinctures[1]->type == 'fur' ? resource_path() . '/img/patterns/' . $this->tinctures[1]->pattern_file_name : $this->tinctures[1]->color;

        $barHeight = (int)ceil($height / 10);

        for ($i = 0; $i < 10; $i++) {
            if ($i % 2 == 0) {
                $tincture = $primaryTincture;
            } else {
                $tincture = $secondaryTincture;
            }

            $tinctureImage = Image::canvas($width, $height, '#000000')->fill($tincture);
            $rectangleImage = Image::canvas($width, $height);

            $rectangleImage->rectangle(0, $i * $barHeight, $width, ($i + 1) * $barHeight, function ($draw) {
                $draw->background('#00ff00');
            });

            $tinctureImage->mask($rectangleImage, true);
            $img->insert($tinctureImage);
        }

        return $img;
    }

    public function renderBlazon(): string
    {
        return 'barry ' . $this->tinctures[0]->name . ' and ' . $this->tinctures[1]->name;
    }
}
