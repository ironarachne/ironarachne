<?php


namespace App\Heraldry\Variation;


use Intervention\Image\Facades\Image;

class ChequyVariation extends Variation
{
    public function __construct()
    {
        $this->name = 'chequy';
        $this->number_of_tinctures = 2;
        $this->blazon = '';
        $this->commonality = 5;
        $this->allow_furs = false;
        $this->tinctures = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $img = Image::canvas($width, $height);

        $primaryTincture = $this->tinctures[0]->type == 'fur' ? resource_path() . '/img/patterns/' . $this->tinctures[0]->pattern_file_name : $this->tinctures[0]->color;
        $secondaryTincture = $this->tinctures[1]->type == 'fur' ? resource_path() . '/img/patterns/' . $this->tinctures[1]->pattern_file_name : $this->tinctures[1]->color;

        $boxSize = (int)ceil($width / 10);

        $i = 0;

        for ($y = 0; $y < ($height / $boxSize) + 1; $y++) {
            for ($x = 0; $x < 10; $x++) {
                if ($i % 2 == 0) {
                    $tincture = $primaryTincture;
                } else {
                    $tincture = $secondaryTincture;
                }

                $tinctureImage = Image::canvas($width, $height, '#000000')->fill($tincture);
                $baseImage = Image::canvas($width, $height);

                $baseImage->rectangle($boxSize * $x, $boxSize * $y, ($x + 1) * $boxSize, ($y + 1) * $boxSize, function ($draw) {
                    $draw->background('#00ff00');
                });

                $tinctureImage->mask($baseImage, true);
                $img->insert($tinctureImage);

                $i++;
            }
            $i++;
        }

        return $img;
    }

    public function renderBlazon(): string
    {
        return 'chequy ' . $this->tinctures[0]->name . ' and ' . $this->tinctures[1]->name;
    }
}
