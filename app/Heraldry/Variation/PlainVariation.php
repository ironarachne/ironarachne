<?php


namespace App\Heraldry\Variation;


use Intervention\Image\Facades\Image;

class PlainVariation extends Variation
{
    public function __construct()
    {
        $this->name = '';
        $this->number_of_tinctures = 1;
        $this->blazon = '';
        $this->commonality = 500;
        $this->allow_furs = true;
        $this->tinctures = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $img = Image::canvas($width, $height);

        if ($this->tinctures[0]->type == 'fur') {
            $img->fill(resource_path() . '/img/patterns/' . $this->tinctures[0]->pattern_file_name);
        } else {
            $img->fill($this->tinctures[0]->color);
        }

        return $img;
    }

    public function renderBlazon(): string
    {
        return $this->tinctures[0]->name;
    }
}
