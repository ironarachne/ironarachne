<?php


namespace App\Heraldry\Variation;


use Intervention\Image\Facades\Image;

class PalyVariation extends Variation
{
    public function __construct()
    {
        $this->name = 'paly';
        $this->number_of_tinctures = 2;
        $this->blazon = '';
        $this->commonality = 5;
        $this->allow_furs = false;
        $this->tinctures = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $img = Image::canvas($width, $height);

        $primaryTincture = $this->tinctures[0]->type == 'fur' ? resource_path() . '/img/patterns/' . $this->tinctures[0]->pattern_file_name : $this->tinctures[0]->color;
        $secondaryTincture = $this->tinctures[1]->type == 'fur' ? resource_path() . '/img/patterns/' . $this->tinctures[1]->pattern_file_name : $this->tinctures[1]->color;

        $barWidth = (int)ceil($width / 6);

        for ($i = 0; $i < 6; $i++) {
            if ($i % 2 == 0) {
                $tincture = $primaryTincture;
            } else {
                $tincture = $secondaryTincture;
            }

            $tinctureImage = Image::canvas($width, $height, '#000000')->fill($tincture);
            $baseImage = Image::canvas($width, $height);

            $baseImage->rectangle($barWidth * $i, 0, ($barWidth * $i) + $barWidth, $height, function ($draw) {
                $draw->background('#00ff00');
            });

            $tinctureImage->mask($baseImage, true);
            $img->insert($tinctureImage);
        }

        return $img;
    }

    public function renderBlazon(): string
    {
        return 'paly ' . $this->tinctures[0]->name . ' and ' . $this->tinctures[1]->name;
    }
}
