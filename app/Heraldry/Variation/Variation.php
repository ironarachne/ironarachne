<?php


namespace App\Heraldry\Variation;

use Intervention\Image\Image;

abstract class Variation
{
    public string $name;
    public string $blazon;
    public int $number_of_tinctures;
    public array $tinctures;
    public int $commonality;
    public bool $allow_furs;

    public abstract function render(int $width, int $height): Image;

    public abstract function renderBlazon(): string;
}
