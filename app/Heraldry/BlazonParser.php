<?php


namespace App\Heraldry;


use App\Exceptions\BlazonParsingException;
use App\Exceptions\MissingChargeException;
use App\Heraldry\Charge\ChargeGroup;
use App\Heraldry\Division\BendDivision;
use App\Heraldry\Division\BendSinisterDivision;
use App\Heraldry\Division\ChevronDivision;
use App\Heraldry\Division\FessDivision;
use App\Heraldry\Division\PaleDivision;
use App\Heraldry\Division\PlainDivision;
use App\Heraldry\Division\QuarterlyDivision;
use App\Heraldry\Division\SaltireDivision;
use App\Heraldry\Field\Field;
use App\Heraldry\Variation\BarryVariation;
use App\Heraldry\Variation\BendySinisterVariation;
use App\Heraldry\Variation\BendyVariation;
use App\Heraldry\Variation\ChequyVariation;
use App\Heraldry\Variation\PalyVariation;
use App\Heraldry\Variation\PlainVariation;
use Illuminate\Support\Str;

class BlazonParser
{
    public function getFieldFromBlazon(string $blazon): Field
    {
        $field = new Field();

        $breakBlazon = explode(',', $blazon);

        $fieldBlazon = $breakBlazon[0];

        $fieldComponents = explode(' ', $fieldBlazon);

        $tinctures = Tincture::all();

        $tinctureNames = [];
        foreach ($tinctures as $tincture) {
            $tinctureNames [] = $tincture->name;
        }

        $variationNames = ['barry', 'bendy', 'chequy', 'paly'];

        if (in_array($fieldComponents[0], $tinctureNames)) {
            $division = new PlainDivision();
            $variation = new PlainVariation();
            $variation->tinctures [] = Tincture::byName($fieldComponents[0]);
            $division->variations [] = $variation;
        } elseif (in_array($fieldComponents[0], $variationNames)) {
            if ($fieldComponents[1] == 'sinister') {
                $variation = new BendySinisterVariation();
                $tinctureName1 = $fieldComponents[2];
                $tinctureName2 = $fieldComponents[4];
            } else {
                $tinctureName1 = $fieldComponents[1];
                $tinctureName2 = $fieldComponents[3];

                if ($fieldComponents[0] == 'barry') {
                    $variation = new BarryVariation();
                } elseif ($fieldComponents[0] == 'bendy') {
                    $variation = new BendyVariation();
                } elseif ($fieldComponents[0] == 'chequy') {
                    $variation = new ChequyVariation();
                } else {
                    $variation = new PalyVariation();
                }
            }
            $division = new PlainDivision();
            $variation->tinctures [] = Tincture::byName($tinctureName1);
            $variation->tinctures [] = Tincture::byName($tinctureName2);
            $division->variations [] = $variation;
        } else {
            $nextComponentIndex = 2;
            if ($fieldComponents[0] == 'quarterly') {
                $division = new QuarterlyDivision();
                $nextComponentIndex = 1;
            } elseif ($fieldComponents[1] == 'bend' && $fieldComponents[2] == 'sinister') {
                $division = new BendSinisterDivision();
                $nextComponentIndex = 3;
            } elseif ($fieldComponents[1] == 'bend') {
                $division = new BendDivision();
            } elseif ($fieldComponents[1] == 'chevron') {
                $division = new ChevronDivision();
            } elseif ($fieldComponents[1] == 'fess') {
                $division = new FessDivision();
            } elseif ($fieldComponents[1] == 'pale') {
                $division = new PaleDivision();
            } else {
                $division = new SaltireDivision();
            }

            // parse the next piece; if it's a variation name, parse as a variation, otherwise parse as a tincture
            // per bend sinister barry argent and Or and bendy gules and vert, three dragons rampant Or
            // per fess argent and barry gules and azure, a sun in splendor Or

            if (in_array($fieldComponents[$nextComponentIndex], $variationNames)) {
                if ($fieldComponents[$nextComponentIndex + 1] == 'sinister') {
                    $variation1 = new BendySinisterVariation();
                    $tinctureName1 = $fieldComponents[$nextComponentIndex + 2];
                    $tinctureName2 = $fieldComponents[$nextComponentIndex + 4];
                    $nextComponentIndex += 6;
                } else {
                    $tinctureName1 = $fieldComponents[$nextComponentIndex + 1];
                    $tinctureName2 = $fieldComponents[$nextComponentIndex + 3];

                    if ($fieldComponents[$nextComponentIndex] == 'barry') {
                        $variation1 = new BarryVariation();
                    } elseif ($fieldComponents[$nextComponentIndex] == 'bendy') {
                        $variation1 = new BendyVariation();
                    } elseif ($fieldComponents[$nextComponentIndex] == 'chequy') {
                        $variation1 = new ChequyVariation();
                    } else {
                        $variation1 = new PalyVariation();
                    }
                    $nextComponentIndex += 5;
                }
                $variation1->tinctures [] = Tincture::byName($tinctureName1);
                $variation1->tinctures [] = Tincture::byName($tinctureName2);
            } else {
                $variation1 = new PlainVariation();
                $variation1->tinctures [] = Tincture::byName($fieldComponents[$nextComponentIndex]);
                $nextComponentIndex += 2;
            }

            $division->variations [] = $variation1;

            if (in_array($fieldComponents[$nextComponentIndex], $variationNames)) {
                if ($fieldComponents[$nextComponentIndex + 1] == 'sinister') {
                    $variation2 = new BendySinisterVariation();
                    $tinctureName1 = $fieldComponents[$nextComponentIndex + 2];
                    $tinctureName2 = $fieldComponents[$nextComponentIndex + 4];
                } else {
                    $tinctureName1 = $fieldComponents[$nextComponentIndex + 1];
                    $tinctureName2 = $fieldComponents[$nextComponentIndex + 3];

                    if ($fieldComponents[$nextComponentIndex] == 'barry') {
                        $variation2 = new BarryVariation();
                    } elseif ($fieldComponents[$nextComponentIndex] == 'bendy') {
                        $variation2 = new BendyVariation();
                    } elseif ($fieldComponents[$nextComponentIndex] == 'chequy') {
                        $variation2 = new ChequyVariation();
                    } else {
                        $variation2 = new PalyVariation();
                    }
                }
                $variation2->tinctures [] = Tincture::byName($tinctureName1);
                $variation2->tinctures [] = Tincture::byName($tinctureName2);
            } else {
                $variation2 = new PlainVariation();
                $variation2->tinctures [] = Tincture::byName($fieldComponents[$nextComponentIndex]);
            }
            $division->variations [] = $variation2;
        }

        $field->division = $division;

        if (sizeof($breakBlazon) > 1) {
            $chargeBlazon = trim($breakBlazon[1]);
            $chargeComponents = explode(' ', $chargeBlazon);

            if ($chargeComponents[0] == 'in') {
                if ($chargeComponents[1] == 'base') {
                    $position = 'in base';
                } else {
                    $position = 'in chief';
                }
                $nextComponentIndex = 2;
            } else {
                $position = 'center';
                $nextComponentIndex = 0;
            }

            $countWord = $chargeComponents[$nextComponentIndex];
            $count = self::parseCount($countWord);

            $nextComponentIndex++;

            $chargeNameComponents = array_slice($chargeComponents, $nextComponentIndex, -1);
            $chargeName = self::parseChargeName($chargeNameComponents, $count);

            try {
                $charge = HeraldryGenerator::getRasterChargeByName($chargeName);
            } catch (MissingChargeException $e) {
                throw new BlazonParsingException($e->getMessage() . ", charge name: $chargeName charge blazon: $chargeBlazon charge name components: " . implode(' ', $chargeNameComponents) . ", count word: $countWord");
            }

            $tinctureName = $chargeComponents[sizeof($chargeComponents) - 1];
            $chargeTincture = Tincture::byName($tinctureName);

            $group = new ChargeGroup($position, $chargeTincture, $count, $charge);

            $field->charge_groups [] = $group;
        }

        return $field;
    }

    private static function parseCount(string $countWord): int {
        if ($countWord == 'three') {
            $count = 3;
        } elseif ($countWord == 'two') {
            $count = 2;
        } elseif (in_array($countWord, ['a', 'an'])) {
            $count = 1;
        } else {
            throw new \Exception("failed to determine count word: $countWord");
        }

        return $count;
    }

    private static function parseChargeName(array $words, int $numberOfCharges): string
    {
        $name = '';
        $madeSingular = false;

        if (sizeof($words) == 1) {
            if ($numberOfCharges > 1) {
                return Str::singular($words[0]);
            }
            return $words[0];
        }

        foreach ($words as $word) {
            if (!$madeSingular && $numberOfCharges > 1 && !self::isWordExceptedFromPluralization($word)) {
                $name .= ' ' . Str::singular($word);
                $madeSingular = true;
            } else {
                $name .= " $word";
            }
        }

        return trim($name);
    }

    private static function isWordExceptedFromPluralization(string $word): bool
    {
        $exceptedWords = [
            'lis',
            'scales',
            'bones',
            'bread',
            'bugle',
            'catherine',
            'dexter',
            'oak',
        ];

        if (str_contains($word, '-')) {
            return true;
        }

        if (str_contains($word, '\'')) {
            return true;
        }

        if (in_array($word, $exceptedWords)) {
            return true;
        }

        return false;
    }
}
