<?php


namespace App\Heraldry\Field;

use App\Point;


class FieldType
{
    public Point $center_point;
    public int $image_width;
    public int $image_height;
    public int $mask_width;
    public int $mask_height;
    public int $top_width;
    public int $center_width;
    public int $bottom_width;
    public string $name;
    public string $mask_file_name;

    public function __construct(Point $center, int $iw, int $ih, int $mw, int $mh,
                                int $topWidth, int $centerWidth, int $bottomWidth, string $name, string $maskFileName)
    {
        $this->center_point = $center;
        $this->image_width = $iw;
        $this->image_height = $ih;
        $this->mask_width = $mw;
        $this->mask_height = $mh;
        $this->top_width = $topWidth;
        $this->center_width = $centerWidth;
        $this->bottom_width = $bottomWidth;
        $this->name = $name;
        $this->mask_file_name = $maskFileName;
    }

    public static function all(): array
    {
        return [
            new FieldType(new Point(250, 300), 500, 600, 450, 550, 350, 350, 350, 'banner', 'banner.png'),
            new FieldType(new Point(250, 300), 500, 600, 450, 550, 350, 350, 350, 'engrailed', 'engrailed.png'),
            new FieldType(new Point(255, 300), 515, 600, 450, 550, 350, 350, 350, 'french', 'french.png'),
            new FieldType(new Point(250, 300), 500, 600, 450, 550, 350, 350, 350, 'heater', 'heater.png'),
            new FieldType(new Point(240, 300), 480, 600, 450, 550, 350, 350, 350, 'square-eared', 'square-eared.png'),
            new FieldType(new Point(241, 300), 482, 600, 450, 550, 280, 300, 280, 'wedge', 'wedge.png'),
        ];
    }

    public static function byName(string $name): FieldType
    {
        $all = self::all();

        foreach ($all as $type) {
            if ($type->name == $name) {
                return $type;
            }
        }

        return $all[0];
    }

    public static function random(): FieldType
    {
        return random_item(self::all());
    }
}
