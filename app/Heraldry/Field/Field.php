<?php


namespace App\Heraldry\Field;

use Intervention\Image\Facades\Image;
use App\Heraldry\Division\Division;

class Field
{
    public Division $division;
    public array $charge_groups;
    public FieldType $field_type;

    public function __construct()
    {
        $this->charge_groups = [];
    }

    public function blazon(): string
    {
        $chargeBlazon = '';

        $fieldBlazon = $this->division->renderBlazon();

        if (sizeof($this->charge_groups) > 0) {
            $fieldBlazon .= ', ';
        }

        foreach ($this->charge_groups as $group) {
            $chargeBlazon .= $group->renderBlazon();
        }

        return $fieldBlazon . $chargeBlazon;
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $img = Image::canvas($this->field_type->image_width, $this->field_type->image_height);

        if ($height > $this->field_type->image_height) {
            $height = $this->field_type->image_height;
            $width = $this->field_type->image_width;
        }

        $divisionImage = $this->division->render($this->field_type->image_width, $this->field_type->image_height);

        $shieldShape = Image::make(resource_path('img/fields/' . $this->field_type->mask_file_name));
        $divisionImage->mask($shieldShape, true);

        $shieldBorder = Image::make(resource_path('img/fields/' . $this->field_type->name . '-lines.png'));
        $divisionImage->insert($shieldBorder);

        $img->insert($divisionImage, 'center');

        foreach ($this->charge_groups as $group) {
            $yOffset = 0;
            $groupImage = $group->render();

            if ($group->position == 'center') {
                $maxWidth = $this->field_type->center_width;
                $position = 'center';
                $maxHeight = $height - 100;
            } elseif ($group->position == 'in chief') {
                $maxWidth = $this->field_type->top_width;
                $position = 'top';
                $maxHeight = (int)floor($height / 4);
                $yOffset = 20;
            } else {
                $maxWidth = $this->field_type->bottom_width;
                $position = 'bottom';
                $maxHeight = (int)floor($height / 3);
                $yOffset = 40;
            }

            $finalImage = $groupImage->widen($maxWidth);
            if ($finalImage->height() > $maxHeight) {
                $finalImage->heighten($maxHeight);
            }

            $img->insert($finalImage, $position, 0, $yOffset);
        }

        $img->widen($width);

        return $img;
    }
}
