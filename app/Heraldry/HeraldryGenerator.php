<?php

namespace App\Heraldry;

use App\Exceptions\MissingChargeException;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;

use App\Heraldry;
use App\Heraldry\Charge\Charge;
use App\Heraldry\Charge\ChargeGroup;
use App\Heraldry\Charge\RasterCharge;
use App\Heraldry\Field\Field;
use App\Heraldry\Field\FieldType;
use App\Heraldry\Division\Division;
use App\Heraldry\Division\BendDivision;
use App\Heraldry\Division\BendSinisterDivision;
use App\Heraldry\Division\FessDivision;
use App\Heraldry\Division\PaleDivision;
use App\Heraldry\Division\PlainDivision;
use App\Heraldry\Division\QuarterlyDivision;
use App\Heraldry\Division\SaltireDivision;
use App\Heraldry\Division\ChevronDivision;
use App\Heraldry\Variation\Variation;
use App\Heraldry\Variation\BendySinisterVariation;
use App\Heraldry\Variation\BendyVariation;
use App\Heraldry\Variation\ChequyVariation;
use App\Heraldry\Variation\PalyVariation;
use App\Heraldry\Variation\PlainVariation;
use App\Heraldry\Variation\BarryVariation;
use App\Tag;

class HeraldryGenerator
{
    public function generate(string $fieldShape = 'any', array $preferredTags = [], array $secondaryTags = [],
                             string $division = 'any', string $primaryTincture = 'any',
                             string $bgTincture1 = 'any', string $bgTincture2 = 'any',
                             string $bgTincture3 = 'any', string $bgTincture4 = 'any',
                             string $haveCharges = 'any', bool $allowVariations = true,
                             string $variation1 = 'any', string $variation2 = 'any'): CoatOfArms
    {
        $fieldTinctures = [];
        $field = new Field();

        if ($fieldShape == 'any') {
            $field->field_type = FieldType::random();
        } else {
            $field->field_type = FieldType::byName($fieldShape);
        }

        if ($division == 'any') {
            $field->division = $this->randomDivision();
        } else {
            $field->division = $this->getDivisionByName($division);
        }

        if ($variation1 == 'any') {
            if ($allowVariations) {
                $field->division->variations [] = $this->randomVariation();
            } else {
                $field->division->variations [] = $this->getVariationByName('plain');
            }
        } else {
            $field->division->variations [] = $this->getVariationByName($variation1);
        }

        if ($field->division->number_of_sections == 2 && $variation2 == 'any') {
            if ($allowVariations) {
                $field->division->variations [] = $this->randomVariation();
            } else {
                $field->division->variations [] = $this->getVariationByName('plain');
            }
        } elseif ($field->division->number_of_sections == 2) {
            $field->division->variations [] = $this->getVariationByName($variation2);
        }

        $allTinctures = Tincture::all();

        if ($primaryTincture == 'any') {
            $tinctures = Tincture::all();
            $tinctures = Tincture::removeFurs($tinctures);
            $chargeTincture = Tincture::randomWeightedFromSet($tinctures);
        } else {
            $chargeTincture = Tincture::byName($primaryTincture);
        }

        $contrastingTinctures = Tincture::contrasting($chargeTincture, $allTinctures);

        if ($bgTincture1 == 'any') {
            $fieldTincture1 = Tincture::randomWeightedFromSet($contrastingTinctures);
        } else {
            $fieldTincture1 = Tincture::byName($bgTincture1);
        }

        $contrastingTinctures = $fieldTincture1->remove($contrastingTinctures);

        if ($bgTincture2 == 'any') {
            $fieldTincture2 = Tincture::randomWeightedFromSet($contrastingTinctures);
        } else {
            $fieldTincture2 = Tincture::byName($bgTincture2);
        }

        if (sizeof($contrastingTinctures) > 1) {
            $contrastingTinctures = $fieldTincture2->remove($contrastingTinctures);
        } else {
            $contrastingTinctures = $allTinctures;
        }

        if ($bgTincture3 == 'any') {
            $fieldTincture3 = Tincture::randomWeightedFromSet($contrastingTinctures);
        } else {
            $fieldTincture3 = Tincture::byName($bgTincture3);
        }

        if (sizeof($contrastingTinctures) > 1) {
            $contrastingTinctures = $fieldTincture3->remove($contrastingTinctures);
        } else {
            $contrastingTinctures = $allTinctures;
        }

        if ($bgTincture4 == 'any') {
            $fieldTincture4 = Tincture::randomWeightedFromSet($contrastingTinctures);
        } else {
            $fieldTincture4 = Tincture::byName($bgTincture4);
        }

        $field->division->variations[0]->tinctures = [$fieldTincture1, $fieldTincture2];

        if (isset($field->division->variations[1])) {
            $field->division->variations[1]->tinctures = [$fieldTincture3, $fieldTincture4];
        }

        $rollForCharges = mt_rand(0, 100);
        if ($haveCharges == 'yes' || ($rollForCharges > 10 && $haveCharges == 'any')) {
            if (!empty($preferredTags) || !empty($secondaryTags)) {
                $chargeGroup = $this->randomChargeGroupWithTags($chargeTincture, $preferredTags, $secondaryTags);
            } else {
                $chargeGroup = $this->randomChargeGroup($chargeTincture);
            }

            $field->charge_groups = [$chargeGroup];
        }

        foreach ($field->division->variations as $variation) {
            foreach ($variation->tinctures as $tincture) {
                $fieldTinctures [] = $tincture;
            }
        }

        $width = 300;
        $height = 450;

        $image = $field->render($width, $height);
        $blazon = $field->blazon();

        $id = md5($blazon);
        $fileName = "$id-{$field->field_type->name}.png";
        $path = 'images/heraldry/devices/' . date('Y/m/d');

        Storage::cloud()->put("$path/$fileName", (string)$image->encode('png'), 'public');
        $imageURL = "https://static.ironarachne.com/$path/$fileName";

        return new CoatOfArms($imageURL, $blazon, $fieldTinctures, $chargeTincture);
    }

    public function random(): CoatOfArms
    {
        return $this->generate();
    }

    private function getDivisionByName(string $name): Division
    {
        if ($name == 'plain') {
            return new PlainDivision();
        } elseif ($name == 'bend-sinister') {
            return new BendSinisterDivision();
        } elseif ($name == 'bend') {
            return new BendDivision();
        } elseif ($name == 'fess') {
            return new FessDivision();
        } elseif ($name == 'quarterly') {
            return new QuarterlyDivision();
        } elseif ($name == 'chevron') {
            return new ChevronDivision();
        } elseif ($name == 'saltire') {
            return new SaltireDivision();
        } elseif ($name == 'pale') {
            return new PaleDivision();
        }

        return new PlainDivision();
    }

    private function getVariationByName(string $name): Variation
    {
        if ($name == 'plain') {
            return new PlainVariation();
        } elseif ($name == 'barry') {
            return new BarryVariation();
        } elseif ($name == 'bendy') {
            return new BendyVariation();
        } elseif ($name == 'bendy-sinister') {
            return new BendySinisterVariation();
        } elseif ($name == 'paly') {
            return new PalyVariation();
        } elseif ($name == 'chequy') {
            return new ChequyVariation();
        }

        return new PlainVariation();
    }

    public function getPennonForDevice(string $blazon): string
    {
        $fileName = md5($blazon) . '-pennon.png';

        $path = 'images/heraldry/devices/' . date('Y/m/d');

        if (!Storage::cloud()->missing("$path/$fileName")) {
            return "https://static.ironarachne.com/$path/$fileName";
        }

        $parser = new BlazonParser();
        $field = $parser->getFieldFromBlazon($blazon);

        if (sizeof($field->division->variations) == 2) {
            $tinctures = [$field->division->variations[0]->tinctures[0], $field->division->variations[1]->tinctures[0]];
        } else {
            if (sizeof($field->division->variations[0]->tinctures) == 2) {
                $tinctures = [$field->division->variations[0]->tinctures[0], $field->division->variations[0]->tinctures[1]];
            } else {
                $tinctures = [$field->division->variations[0]->tinctures[0]];
            }
        }

        $mainField = Image::canvas(549, 150);

        if ($tinctures[0]->type == 'fur') {
            $mainField->fill(resource_path() . '/img/patterns/' . $tinctures[0]->pattern_file_name);
        } else {
            $mainField->fill($tinctures[0]->color);
        }

        if (sizeof($tinctures) == 2) {
            $secondField = Image::canvas(549, 75);

            if ($tinctures[1]->type == 'fur') {
                $secondField->fill(resource_path() . '/img/patterns/' . $tinctures[1]->pattern_file_name);
            } else {
                $secondField->fill($tinctures[1]->color);
            }

            $mainField->insert($secondField, 'bottom');
        }

        $pennonShape = Image::make(resource_path('img/fields/pennon.png'));
        $mainField->mask($pennonShape, true);

        $pennonBorder = Image::make(resource_path('img/fields/pennon-lines.png'));
        $mainField->insert($pennonBorder);

        if (sizeof($field->charge_groups) > 0) {
            $charge = $field->charge_groups[0]->charge;
            $charge->tincture = $field->charge_groups[0]->tincture;
            $chargeImage = $charge->render();
            $chargeImage->heighten(80);

            $mainField->insert($chargeImage, 'left', 20);
        }

        Storage::cloud()->put("$path/$fileName", (string)$mainField->encode('png'), 'public');
        return "https://static.ironarachne.com/$path/$fileName";
    }

    public static function getRasterChargeByName(string $name): RasterCharge
    {
        $charges = self::loadRasterCharges();

        foreach ($charges as $charge) {
            if ($charge->name == $name) {
                return $charge;
            }
        }

        throw new MissingChargeException($name);
    }

    public static function loadRasterCharges(): array
    {
        return Cache::remember('charges_all', 600, function () {
            $charges = [];

            $url = env('DATA_CORE_URL');

            $client = new Client();
            $response = $client->request('GET', $url . '/charges');

            $data = json_decode($response->getBody()->getContents());

            foreach ($data->charges as $c) {
                $tags = [];
                foreach ($c->tags as $t) {
                    $tag = Tag::fromObject($t);
                    $tags [] = $tag;
                }
                if (!isset($c->descriptor) || empty($c->descriptor)) {
                    $c->descriptor = '';
                }
                $charge = new RasterCharge($c->identifier, $c->name, $c->noun, $c->noun_plural, $c->descriptor, $c->single_only, $tags);
                $charges [] = $charge;
            }

            if (sizeof($charges) == 0) {
                throw new MissingChargeException('all charges are missing');
            }

            return $charges;
        });
    }

    private function randomCharge(): Charge
    {
        $all = self::loadRasterCharges();

        return random_item($all);
    }

    private function randomChargeWithTags(array $preferred, array $secondary): Charge
    {
        $charges = [];

        $all = self::loadRasterCharges();

        foreach ($all as $charge) {
            foreach ($charge->tags as $tag) {
                if (in_array($tag->name, $preferred)) {
                    $charges [] = $charge;
                }
            }
        }

        if (empty($charges)) {
            foreach ($all as $charge) {
                foreach ($charge->tags as $tag) {
                    if (in_array($tag->name, $secondary)) {
                        $charges [] = $charge;
                    }
                }
            }
        }

        if (empty($charges)) {
            return $this->randomCharge();
        }

        return random_item($charges);
    }

    private function randomChargeGroup(Tincture $tincture): ChargeGroup
    {
        $chargeWeights = [
            '1' => 100,
            '2' => 50,
            '3' => 33,
        ];

        $numberOfCharges = (int)random_weighted_item($chargeWeights);

        $charge = $this->randomCharge();

        $position = 'center';

        return new ChargeGroup($position, $tincture, $numberOfCharges, $charge);
    }

    private function randomChargeGroupWithTags(Tincture $tincture, array $preferredTags, array $secondaryTags): ChargeGroup
    {
        $chargeWeights = [
            '1' => 100,
            '2' => 50,
            '3' => 33,
        ];

        $numberOfCharges = (int)random_weighted_item($chargeWeights);

        $charge = $this->randomChargeWithTags($preferredTags, $secondaryTags);

        $position = 'center';

        return new ChargeGroup($position, $tincture, $numberOfCharges, $charge);
    }

    private function randomDivision(): Division
    {
        $weights = [];

        $all = [
            new BendDivision(),
            new BendSinisterDivision(),
            new ChevronDivision(),
            new FessDivision(),
            new PaleDivision(),
            new PlainDivision(),
            new QuarterlyDivision(),
            new SaltireDivision(),
        ];

        foreach ($all as $d) {
            $weights [$d->name] = $d->commonality;
        }

        $divisionName = random_weighted_item($weights);

        foreach ($all as $d) {
            if ($d->name == $divisionName) {
                $division = $d;
            }
        }

        return $division;
    }

    private function randomVariation(): Variation
    {
        $weights = [];

        $all = [
            new BarryVariation(),
            new BendyVariation(),
            new BendySinisterVariation(),
            new ChequyVariation(),
            new PalyVariation(),
            new PlainVariation(),
        ];

        foreach ($all as $v) {
            $weights [$v->name] = $v->commonality;
        }

        $variationName = random_weighted_item($weights);

        foreach ($all as $v) {
            if ($v->name == $variationName) {
                return $v;
            }
        }
    }
}
