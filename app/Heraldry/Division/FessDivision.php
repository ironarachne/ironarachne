<?php


namespace App\Heraldry\Division;


use Intervention\Image\Facades\Image;

class FessDivision extends Division
{
    public function __construct()
    {
        $this->name = 'fess';
        $this->number_of_sections = 2;
        $this->commonality = 2;
        $this->variations = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $image1 = $this->variations[0]->render($width, $height);
        $image2 = $this->variations[1]->render($width, $height);

        $image = Image::canvas($width, $height)->fill($image1);

        $fess = Image::canvas($width, $height)->rectangle(0, floor($height / 2), $width, $height, function ($draw) {
            $draw->background('#00ff00');
        });

        $image2->mask($fess, true);

        $image->insert($image2, 'bottom-left');

        return $image;
    }

    public function renderBlazon(): string
    {
        $blazon1 = $this->variations[0]->renderBlazon();
        $blazon2 = $this->variations[1]->renderBlazon();

        return "per fess $blazon1 and $blazon2";
    }
}
