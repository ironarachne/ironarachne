<?php


namespace App\Heraldry\Division;


use Intervention\Image\Facades\Image;

class PaleDivision extends Division
{
    public function __construct()
    {
        $this->name = 'pale';
        $this->number_of_sections = 2;
        $this->commonality = 2;
        $this->variations = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $image1 = $this->variations[0]->render($width, $height);
        $image2 = $this->variations[1]->render($width, $height);

        $image = Image::canvas($width, $height)->fill($image1);

        $pale = Image::canvas($width, $height)->rectangle(floor($width / 2), 0, $width, $height, function ($draw) {
            $draw->background('#00ff00');
        });

        $image2->mask($pale, true);

        $image->insert($image2, 'right');

        return $image;
    }

    public function renderBlazon(): string
    {
        $blazon1 = $this->variations[0]->renderBlazon();
        $blazon2 = $this->variations[1]->renderBlazon();

        return "per pale $blazon1 and $blazon2";
    }
}
