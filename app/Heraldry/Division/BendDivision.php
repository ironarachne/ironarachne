<?php


namespace App\Heraldry\Division;


use Intervention\Image\Facades\Image;

class BendDivision extends Division
{
    public function __construct()
    {
        $this->name = 'bend';
        $this->number_of_sections = 2;
        $this->commonality = 2;
        $this->variations = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $image1 = $this->variations[0]->render($width, $height);
        $image2 = $this->variations[1]->render($width, $height);

        $image = Image::canvas($width, $height)->fill($image1);

        $bend = Image::canvas($width, $height)->polygon([0, 0, $width, 0, $width, $height, 0, 0], function ($draw) {
            $draw->background('#00ff00');
        });

        $image2->mask($bend, true);

        $image->insert($image2);

        return $image;
    }

    public function renderBlazon(): string
    {
        $blazon1 = $this->variations[0]->renderBlazon();
        $blazon2 = $this->variations[1]->renderBlazon();

        return "per bend $blazon1 and $blazon2";
    }
}
