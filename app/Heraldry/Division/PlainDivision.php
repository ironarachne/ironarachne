<?php


namespace App\Heraldry\Division;

class PlainDivision extends Division
{
    public function __construct()
    {
        $this->name = 'plain';
        $this->number_of_sections = 1;
        $this->commonality = 2;
        $this->variations = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        return $this->variations[0]->render($width, $height);
    }

    public function renderBlazon(): string
    {
        return $this->variations[0]->renderBlazon();
    }
}
