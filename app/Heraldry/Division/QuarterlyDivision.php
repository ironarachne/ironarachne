<?php


namespace App\Heraldry\Division;


use Intervention\Image\Facades\Image;

class QuarterlyDivision extends Division
{
    public function __construct()
    {
        $this->name = 'quarterly';
        $this->number_of_sections = 2;
        $this->commonality = 2;
        $this->variations = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $image1 = $this->variations[0]->render($width, $height);
        $image2 = $this->variations[1]->render($width, $height);
        $image3 = $this->variations[1]->render($width, $height);

        $image = Image::canvas($width, $height)->fill($image1);

        $square1 = Image::canvas($width, $height)->rectangle(floor($width / 2), 0, $width, floor($height / 2), function ($draw) {
            $draw->background('#00ff00');
        });

        $square2 = Image::canvas($width, $height)->rectangle(0, floor($height / 2), floor($width / 2), $height, function ($draw) {
            $draw->background('#00ff00');
        });

        $image2->mask($square1, true);
        $image3->mask($square2, true);

        $image->insert($image2, 'top-left');
        $image->insert($image3, 'bottom-right');

        return $image;
    }

    public function renderBlazon(): string
    {
        $blazon1 = $this->variations[0]->renderBlazon();
        $blazon2 = $this->variations[1]->renderBlazon();

        return "quarterly $blazon1 and $blazon2";
    }
}
