<?php


namespace App\Heraldry\Division;


use Intervention\Image\Facades\Image;

class SaltireDivision extends Division
{
    public function __construct()
    {
        $this->name = 'saltire';
        $this->number_of_sections = 2;
        $this->commonality = 2;
        $this->variations = [];
    }

    public function render(int $width, int $height): \Intervention\Image\Image
    {
        $image1 = $this->variations[0]->render($width, $height);
        $image2 = $this->variations[1]->render($width, $height);
        $image3 = $this->variations[1]->render($width, $height);

        $image = Image::canvas($width, $height)->fill($image1);

        $triangle1 = Image::canvas($width, $height)->polygon([0, 0, $width, 0, floor($width / 2), floor($height / 2)], function ($draw) {
            $draw->background('#00ff00');
        });

        $triangle2 = Image::canvas($width, $height)->polygon([0, $height, floor($width / 2), floor($height / 2), $width, $height], function ($draw) {
            $draw->background('#00ff00');
        });

        $image2->mask($triangle1, true);
        $image3->mask($triangle2, true);

        $image->insert($image2, 'top');
        $image->insert($image3, 'bottom');

        return $image;
    }

    public function renderBlazon(): string
    {
        $blazon1 = $this->variations[0]->renderBlazon();
        $blazon2 = $this->variations[1]->renderBlazon();

        return "per saltire $blazon1 and $blazon2";
    }
}
