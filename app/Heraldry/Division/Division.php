<?php


namespace App\Heraldry\Division;


use Intervention\Image\Image;

abstract class Division
{
    public string $name;
    public string $blazon;
    public int $number_of_sections;
    public array $variations;
    public int $commonality;

    public abstract function render(int $width, int $height): Image;
    public abstract function renderBlazon(): string;
}
