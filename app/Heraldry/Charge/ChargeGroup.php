<?php


namespace App\Heraldry\Charge;

use App\Heraldry\Tincture;
use Intervention\Image\Facades\Image;

class ChargeGroup
{
    public string $position;
    public Tincture $tincture;
    public int $number_of_charges;
    public Charge $charge;

    public function __construct(string $position, Tincture $tincture, int $numCharges, Charge $charge)
    {
        $this->position = $position;
        $this->tincture = $tincture;
        $this->number_of_charges = $numCharges;
        $this->charge = $charge;
    }

    public function render(): \Intervention\Image\Image
    {
        $this->charge->tincture = $this->tincture;
        $chargeImage = $this->charge->render();

        $imageWidth = $chargeImage->width();
        $imageHeight = $chargeImage->height();

        if ($this->number_of_charges == 3) {
            $orientation = 'horizontal';

            if ($this->position == 'center') {
                $orientation = random_item(['vertical', 'horizontal', 'triangle']);
            } elseif ($this->position == 'in chief') {
                $orientation = 'horizontal';
            } elseif ($this->position == 'in base') {
                $orientation = 'triangle';
            }

            if ($orientation == 'vertical') {
                $image = Image::canvas($imageWidth, $imageHeight * 3.5);
                $image->insert($chargeImage, 'top');
                $image->insert($chargeImage, 'center');
                $image->insert($chargeImage, 'bottom');
                return $image;
            } elseif ($orientation == 'horizontal') {
                $image = Image::canvas($imageWidth * 3.5, $imageHeight);
                $image->insert($chargeImage, 'left');
                $image->insert($chargeImage, 'center');
                $image->insert($chargeImage, 'right');
                return $image;
            } else {
                $image = Image::canvas($imageWidth * 2.5, $imageHeight * 2.25);
                $image->insert($chargeImage, 'top-left');
                $image->insert($chargeImage, 'top-right');
                $image->insert($chargeImage, 'bottom');
                return $image;
            }
        } elseif ($this->number_of_charges == 2) {
            $orientation = random_item(['vertical', 'horizontal']);

            if ($this->position == 'in chief') {
                $orientation = 'horizontal';
            } elseif ($this->position == 'in base') {
                $orientation = 'vertical';
            }

            if ($orientation == 'vertical') {
                $image = Image::canvas($imageWidth, $imageHeight * 2.25);
                $image->insert($chargeImage, 'top');
                $image->insert($chargeImage, 'bottom');
                return $image;
            } else {
                $image = Image::canvas($imageWidth * 2.25, $imageHeight);
                $image->insert($chargeImage, 'left');
                $image->insert($chargeImage, 'right');
                return $image;
            }
        } else {
            return $chargeImage;
        }
    }

    public function renderBlazon(): string
    {
        if ($this->position == 'in chief') {
            $position = 'in chief ';
        } else if ($this->position == 'in base') {
            $position = 'in base ';
        } else {
            $position = '';
        }

        if ($this->number_of_charges == 1) {
            $pronoun = article($this->charge->name);
            $charge = $this->charge->noun;
            if ($this->charge->descriptor != '') {
                $charge .= ' ' . $this->charge->descriptor;
            }
            $tincture = $this->tincture->name;

            return "$position$pronoun $charge $tincture";
        } elseif ($this->number_of_charges == 2) {
            $count = 'two';
        } elseif ($this->number_of_charges == 3) {
            $count = 'three';
        }

        $charge = $this->charge->noun_plural;
        if ($this->charge->descriptor != '') {
            $charge .= ' ' . $this->charge->descriptor;
        }
        $tincture = $this->tincture->name;

        return "$position$count $charge $tincture";
    }
}
