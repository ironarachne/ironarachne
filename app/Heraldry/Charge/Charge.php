<?php


namespace App\Heraldry\Charge;

use App\Heraldry\Tincture;
use Intervention\Image\Image;

abstract class Charge
{
    public string $identifier;
    public string $name;
    public string $noun;
    public string $noun_plural;
    public string $descriptor;
    public Tincture $tincture;
    public bool $single_only;
    public array $tags;

    public abstract function render(): Image;
}
