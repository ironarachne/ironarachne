<?php


namespace App\Heraldry\Charge;


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class RasterCharge extends Charge
{
    public function __construct(string $identifier, string $name, string $noun, string $nounPlural, string $descriptor, bool $singleOnly, array $tags)
    {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->noun = $noun;
        $this->noun_plural = $nounPlural;
        $this->descriptor = $descriptor;
        $this->single_only = $singleOnly;
        $this->tags = $tags;
    }

    public function render(): \Intervention\Image\Image
    {
        $maskFileName = $this->identifier . '.png';
        $linesFileName = $this->identifier . '-lines.png';

        if (Storage::disk('local')->missing('charges/' . $maskFileName)) {
            $data = Storage::cloud()->get('/images/heraldry/sources/charges/' . $maskFileName);
            Storage::put('charges/' . $maskFileName, $data);
        }

        if (Storage::disk('local')->missing('charges/' . $linesFileName)) {
            $data = Storage::cloud()->get('/images/heraldry/sources/charges/' . $linesFileName);
            Storage::put('charges/' . $linesFileName, $data);
        }

        $tincture = $this->tincture->type == 'fur' ? resource_path('img/patterns/' . $this->tincture->pattern_file_name) : $this->tincture->color;

        $chargeImage = Image::make(Storage::get('charges/' . $maskFileName));

        $image = Image::canvas($chargeImage->width(), $chargeImage->height())->fill($tincture);

        $image->mask($chargeImage, true);

        $linesImage = Image::make(Storage::get('charges/' . $linesFileName));

        if ($this->tincture->name == 'sable') {
            $linesImage->invert();
        }

        $image->insert($linesImage);

        return $image;
    }
}
