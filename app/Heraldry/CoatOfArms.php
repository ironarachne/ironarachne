<?php


namespace App\Heraldry;


class CoatOfArms
{
    public string $image_url;
    public string $blazon;
    public array $field_tinctures;
    public Tincture $charge_tincture;

    public function __construct(string $url, string $blazon, array $fieldTinctures, Tincture $chargeTincture)
    {
        $this->image_url = $url;
        $this->blazon = $blazon;
        $this->field_tinctures = $fieldTinctures;
        $this->charge_tincture = $chargeTincture;
    }
}
