<?php


namespace App\Heraldry;

use App\Tag;


class Tincture
{
    public string $name;
    public string $type;
    public string $color;
    public string $pattern_file_name;
    public array $tags;
    public int $commonality;

    public function __construct(string $name, string $type, string $color, string $patternFileName, array $tags, int $commonality)
    {
        $this->name = $name;
        $this->type = $type;
        $this->color = $color;
        $this->pattern_file_name = $patternFileName;
        $this->tags = $tags;
        $this->commonality = $commonality;
    }

    public static function all(): array
    {
        $colorTag = new Tag();
        $colorTag->name = 'color';

        $furTag = new Tag();
        $furTag->name = 'fur';

        $metalTag = new Tag();
        $metalTag->name = 'metal';

        $stainTag = new Tag();
        $stainTag->name = 'stain';

        return [
            new Tincture('azure', 'color', '#0731BA', '', [$colorTag], 100),
            new Tincture('gules', 'color', '#D40D02', '', [$colorTag], 100),
            new Tincture('purpure', 'color', '#6131B5', '', [$colorTag], 100),
            new Tincture('sable', 'color', '#000000', '', [$colorTag], 100),
            new Tincture('vert', 'color', '#0B731B', '', [$colorTag], 100),
            new Tincture('erminois', 'fur', '#ffaa00', 'erminois.png', [$furTag], 5),
            new Tincture('ermine', 'fur', '#ffffff', 'ermine.png', [$furTag], 6),
            new Tincture('ermines', 'fur', '#000000', 'ermines.png', [$furTag], 5),
            new Tincture('pean', 'fur', '#000000', 'pean.png', [$furTag], 4),
            new Tincture('argent', 'metal', '#ffffff', '', [$metalTag], 95),
            new Tincture('Or', 'metal', '#F0D41F', '', [$metalTag], 95),
            new Tincture('murrey', 'stain', '#731842', '', [$stainTag], 5),
            new Tincture('sanguine', 'stain', '#8C1508', '', [$stainTag], 5),
            new Tincture('tenné', 'stain', '#C79C4D', '', [$stainTag], 5),
        ];
    }

    public function complements(Tincture $tincture): bool
    {
        if ($this->type == 'color' && $tincture->type == 'metal') {
            return false;
        } elseif ($this->type == 'metal' && in_array($tincture->type, ['color', 'stain'])) {
            return false;
        } elseif ($this->name == $tincture->name) {
            return false;
        }

        return true;
    }

    public function contrasts(Tincture $tincture): bool
    {
        if (($this->type == 'color' || $this->type == 'stain') && $tincture->type == 'metal') {
            return true;
        } elseif ($this->type == 'metal' && in_array($tincture->type, ['color', 'stain'])) {
            return true;
        } elseif ($this->type == 'fur') {
            return true;
        } elseif ($this->name == $tincture->name) {
            return false;
        }

        return false;
    }

    public function remove(array $haystack): array
    {
        $result = [];

        foreach ($haystack as $t) {
            if ($this->name != $t->name) {
                $result[] = $t;
            }
        }

        return $result;
    }

    public static function byName(string $name): Tincture
    {
        $all = self::all();

        foreach ($all as $t) {
            if ($t->name == $name) {
                return $t;
            }
        }

        return Tincture::randomAll();
    }

    public static function byTag(Tag $tag): array
    {
        $all = self::all();

        $result = [];

        foreach ($all as $t) {
            if ($tag->in($t->tags)) {
                $result[] = $t;
            }
        }

        return $result;
    }

    public static function complementary(Tincture $tincture, array $haystack): array
    {
        $result = [];

        foreach ($haystack as $t) {
            if ($tincture->complements($t)) {
                $result[] = $t;
            }
        }

        return $result;
    }

    public static function contrasting(Tincture $tincture, array $haystack): array
    {
        $result = [];

        foreach ($haystack as $t) {
            if ($tincture->contrasts($t)) {
                $result[] = $t;
            }
        }

        return $result;
    }

    public static function getColors(): array
    {
        $all = self::all();

        $result = [];

        foreach ($all as $t) {
            if ($t->type == 'color') {
                $result[] = $t;
            }
        }

        return $result;
    }

    public static function getFurs(): array
    {
        $all = self::all();

        $result = [];

        foreach ($all as $t) {
            if ($t->type == 'fur') {
                $result[] = $t;
            }
        }

        return $result;
    }

    public static function getMetals(): array
    {
        $all = self::all();

        $result = [];

        foreach ($all as $t) {
            if ($t->type == 'metal') {
                $result[] = $t;
            }
        }

        return $result;
    }

    public static function getStains(): array
    {
        $all = self::all();

        $result = [];

        foreach ($all as $t) {
            if ($t->type == 'stain') {
                $result[] = $t;
            }
        }

        return $result;
    }

    public static function randomAll(): Tincture
    {
        return random_item(self::all());
    }

    public static function randomFromSet(array $tinctures): Tincture
    {
        return random_item($tinctures);
    }

    public static function randomWeighted(): Tincture
    {
        $weights = [];

        $all = self::all();
        foreach ($all as $t) {
            $weights[$t->name] = $t->commonality;
        }

        $tinctureName = random_weighted_item($weights);

        foreach ($all as $t) {
            if ($t->name == $tinctureName) {
                return $t;
            }
        }

        return $all[0];
    }

    public static function randomWeightedFromSet(array $tinctures): Tincture
    {
        $weights = [];

        foreach ($tinctures as $t) {
            $weights[$t->name] = $t->commonality;
        }

        $tinctureName = random_weighted_item($weights);

        foreach ($tinctures as $t) {
            if ($t->name == $tinctureName) {
                return $t;
            }
        }

        return $tinctures[0];
    }

    public static function removeFurs(array $tinctures): array
    {
        $result = [];

        foreach ($tinctures as $t) {
            if ($t->type != 'fur') {
                $result [] = $t;
            }
        }

        return $result;
    }
}
