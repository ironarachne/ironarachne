<?php


namespace App\Math;


use MapGenerator\PerlinNoiseGenerator;

class PerlinNoise
{
    private PerlinNoiseGenerator $generator;
    private int $width;
    private int $height;

    public function __construct(int $width, int $height) {
        $this->generator = new PerlinNoiseGenerator();
        $this->generator->setSize($width);
        $this->generator->setPersistence(0.8);
        $this->width = $width;
        $this->height = $height;
    }

    public function generate(): array
    {
        $generated = $this->generator->generate();

        $max = 0;
        $min = PHP_INT_MAX;
        for ($iy = 0; $iy < $this->height; $iy++) {
            for ($ix = 0; $ix < $this->width; $ix++) {
                $h = $generated[$iy][$ix];
                if ($min > $h) {
                    $min = $h;
                }
                if ($max < $h) {
                    $max = $h;
                }
            }
        }
        $diff = $max - $min;

        $result = [];

        for ($y = 0; $y < sizeof($generated); $y++) {
            for ($x = 0; $x < sizeof($generated[$y]); $x++) {
                $result[$y][$x] = ($generated[$y][$x] - $min) / $diff;
            }
        }

        return $result;
    }
}
