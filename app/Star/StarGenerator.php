<?php


namespace App\Star;


use App\Graphics\StarField;
use Illuminate\Support\Facades\Storage;
use Imagick;
use Intervention\Image\Facades\Image;

class StarGenerator
{
    public function generate(string $type = 'random', int $imageWidth = 128, int $imageHeight = 128): Star
    {
        if ($type == 'random') {
            $starType = $this->getRandomType();
        } else {
            $starType = $this->getTypeByName($type);
        }

        $star = new Star();
        $star->type = $starType;

        $min = floor($star->type->size_min * min($imageWidth, $imageHeight));
        $max = floor($star->type->size_max * min($imageWidth, $imageHeight));

        $starSize = mt_rand($min, $max);

        $image = $this->render($starType, $imageWidth, $imageHeight, $starSize, $starType->glow_strength, $starType->glow_color);

        $id = md5($starType->name . microtime());
        $fileName = "$id.png";
        $path = 'images/stars/' . date('Y/m/d');

        Storage::cloud()->put("$path/$fileName", (string)$image->encode('png'), 'public');
        $star->image_url = "https://static.ironarachne.com/$path/$fileName";

        return $star;
    }

    private function render(StarType $type, int $width, int $height, int $starSize, int $glowStrength, string $glowColor): \Intervention\Image\Image
    {
        $canvas = Image::canvas($width, $height);

        $texture = $type->getTexture($width, $height)->blur(2);

        $imagick = $texture->getCore();
        $controlPoints = [0.4, 0.0, 0.0, 0.8];
        $imagick->distortImage(Imagick::DISTORTION_BARREL, $controlPoints, true);
        $texture = Image::make($imagick);

        $starMask = Image::canvas($width, $height);
        $starMask->circle($starSize, $width / 2, $height / 2, function ($draw) {
            $draw->background('#000000');
        });

        $texture->mask($starMask, true);

        $glow = Image::canvas($width, $height);
        $glow->circle($starSize + $glowStrength, $width / 2, $height / 2, function ($draw) use ($glowColor) {
            $draw->background($glowColor);
        })->blur(20);

        $canvas->insert($glow);
        $canvas->insert($texture);

        $starField = StarField::generate($width, $height);

        $starField->insert($canvas);

        return $starField;
    }

    private function getAllTypes(): array
    {
        return [
            new BlueStarType(),
            new RedStarType(),
            new YellowStarType(),
            new WhiteStarType(),
        ];
    }

    private function getRandomType(): StarType
    {
        return random_item($this->getAllTypes());
    }

    private function getTypeByName(string $name): StarType
    {
        $types = $this->getAllTypes();

        foreach ($types as $type) {
            if ($type->name == $name) {
                return $type;
            }
        }
    }
}
