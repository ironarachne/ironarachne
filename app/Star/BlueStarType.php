<?php


namespace App\Star;


use App\Math\PerlinNoise;
use Imagick;
use Intervention\Image\Image;

class BlueStarType extends StarType
{
    public function __construct() {
        $this->name = 'blue';
        $this->glow_color = '#0011FF';
        $this->glow_strength = 3;
        $this->size_min = 0.2;
        $this->size_max = 0.4;
    }

    public function getTexture(int $width, int $height): Image
    {
        $noiseGen = new PerlinNoise($width, $height);
        $noise = $noiseGen->generate();
        $noise2 = $noiseGen->generate();

        $img = new Imagick();
        $img->newImage($width, $height, 'rgb(0,0,0)');

        $iterator = $img->getPixelIterator();

        foreach ($iterator as $row => $pixels) {
            foreach ($pixels as $col => $pixel) {
                $h = $noise2[$row][$col] * 255;
                if ($h > 180) {
                    $r = (20 * $noise[$row][$col]);
                    $g = (20 * $noise[$row][$col]);
                    $b = (100 * $noise[$row][$col]) + 100;
                } else {
                    $r = (20 * $noise[$row][$col]);
                    $g = (50 * $noise[$row][$col]);
                    $b = (50 * $noise[$row][$col]) + 205;
                }
                $pixel->setColor("rgb($r, $g, $b)");
            }
            $iterator->syncIterator();
        }

        return \Intervention\Image\Facades\Image::make($img);
    }
}
