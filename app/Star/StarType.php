<?php


namespace App\Star;


abstract class StarType
{
    public string $name;
    public string $glow_color;
    public int $glow_strength;
    public float $size_min;
    public float $size_max;

    abstract public function getTexture(int $width, int $height);
}
