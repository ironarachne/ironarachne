<?php


namespace App\Star;


class Star
{
    public string $name;
    public StarType $type;
    public string $image_url;
}
