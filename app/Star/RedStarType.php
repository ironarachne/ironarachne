<?php


namespace App\Star;


use App\Math\PerlinNoise;
use Imagick;
use Intervention\Image\Image;

class RedStarType extends StarType
{
    public function __construct() {
        $this->name = 'red';
        $this->glow_color = '#FF0000';
        $this->glow_strength = 3;
        $this->size_min = 0.8;
        $this->size_max = 1.0;
    }

    public function getTexture(int $width, int $height): Image
    {
        $noiseGen = new PerlinNoise($width, $height);
        $noise = $noiseGen->generate();
        $noise2 = $noiseGen->generate();

        $img = new Imagick();
        $img->newImage($width, $height, 'rgb(0,0,0)');

        $iterator = $img->getPixelIterator();

        foreach ($iterator as $row => $pixels) {
            foreach ($pixels as $col => $pixel) {
                $h = $noise2[$row][$col] * 255;
                if ($h > 180) {
                    $r = (100 * $noise[$row][$col]) + 100;
                    $g = (20 * $noise[$row][$col]);
                    $b = (20 * $noise[$row][$col]);
                } else {
                    $r = (50 * $noise[$row][$col]) + 205;
                    $g = (50 * $noise[$row][$col]);
                    $b = (20 * $noise[$row][$col]);
                }
                $pixel->setColor("rgb($r, $g, $b)");
            }
            $iterator->syncIterator();
        }

        return \Intervention\Image\Facades\Image::make($img);
    }
}
