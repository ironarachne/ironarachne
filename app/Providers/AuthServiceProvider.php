<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-stats', function ($user) {
            return $user->is_admin;
        });

        Gate::define('create-user', function ($user) {
            return $user->is_admin;
        });

        Gate::define('delete-user', function ($user, $target) {
            return $user->is_admin || $user->id == $target->id;
        });

        Gate::define('edit-user', function ($user, $target) {
            return $user->is_admin || $user->id == $target->id;
        });

        Gate::define('list-users', function ($user) {
            return $user->is_admin;
        });

        Gate::define('set-heraldry-for-user', function ($user, $target) {
            return $user->id == $target->id;
        });
    }
}
