<?php


namespace App\NameGenerator;


class NameGenerator
{
    public array $female_first_names;
    public array $female_last_names;
    public array $male_first_names;
    public array $male_last_names;
    public array $place_names;
    public array $culture_names;

    public function generate($gender, $withLastName = true): string
    {
        $first = $this->female_first_names;
        $last = $this->female_last_names;

        if ($gender == 'male') {
            $first = $this->male_first_names;
            $last = $this->male_last_names;
        }

        $name = random_item($first);

        if ($withLastName) {
            $name .= ' ' . random_item($last);
        }

        return $name;
    }

    public function randomCultureName(): string
    {
        return random_item($this->culture_names);
    }

    public function randomPlaceName(): string
    {
        return random_item($this->place_names);
    }

    public function randomSetOfNames(string $type, int $number): array
    {
        $names = [];

        if ($type == 'female first') {
            $options = $this->female_first_names;
        } elseif ($type == 'female last') {
            $options = $this->female_last_names;
        } elseif ($type == 'male first') {
            $options = $this->male_first_names;
        } elseif ($type == 'male last') {
            $options = $this->male_last_names;
        } elseif ($type == 'place') {
            $options = $this->place_names;
        } elseif ($type == 'culture') {
            $options = $this->culture_names;
        } else {
            $options = array_merge($this->female_first_names, $this->male_first_names);
        }

        $max = $number > sizeof($options) ? sizeof($options) : $number;

        for ($i=0; $i<$max; $i++) {
            $name = random_item($options);
            if (!in_array($name, $names)) {
                $names [] = $name;
            }
        }

        return $names;
    }
}
