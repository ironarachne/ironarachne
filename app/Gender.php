<?php


namespace App;


class Gender
{
    public static function random(): string {
        $items = ['male', 'female'];

        return random_item($items);
    }
}
