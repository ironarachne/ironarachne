<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSavedOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saved_organizations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('user_id')->nullable();
            $table->string('guid', 128)->unique();
            $table->string('name');
            $table->string('description');
            $table->text('html');
            $table->json('data');
            $table->index('created_at', 'ix_created');
            $table->index(['user_id', 'created_at'], 'ix_user_created');
            $table->index('user_id', 'ix_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saved_organizations');
    }
}
