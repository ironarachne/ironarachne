<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClaimantIdToHeraldries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('heraldries', function (Blueprint $table) {
            $table->unsignedBigInteger('claimant_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('heraldries', function (Blueprint $table) {
            $table->dropColumn('claimant_id');
        });
    }
}
