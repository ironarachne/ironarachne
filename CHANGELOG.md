# Change Log

## 1.6.3 (2020-10-26)

- Remove another reference to ben@ironarachne.com

## 1.6.2 (2020-10-26)

- Remove references to ben@ironarachne.com email

## 1.6.1 (2020-10-07)

- Added seed parameter for star system generator

## 1.6.0 (2020-10-07)

- Add star system generator

## 1.5.0 (2020-09-28)

- Add biome choice to region and culture generators
- Add user profiles
- Add admin stats screen
- Add user heraldry
- Add user admin screen
- Change language selection to be a dropdown

## 1.4.6 (2020-09-26)

- Fix API endpoints for name generation

## 1.4.5 (2020-09-25)

- Move NameGenerator to its own namespace
- Refactor "defaultFantasy" name generator as its own class that extends NameGenerator
- Enhance the CommonFantasy place name generation process

## 1.4.4 (2020-09-25)

- Add default plants in case filtering eliminates them all
- Fix charge name parsing bugs with regard to pluralization

## 1.4.3 (2020-09-25)

- Fix missing charge images on "all charges" page
- Remove unique url index from heraldries database
- Add default food bases
- Add default deity appearance traits
- Add Gender class
- Add exception class for missing charges
- Add exception class for blazon parsing errors
- Add BlazonParser class

## 1.4.2 (2020-09-23)

- Add "Buy me a coffee" button

## 1.4.1 (2020-09-18)

- Fix error in heraldry controller with null fieldshape

## 1.4.0 (2020-09-14)

- Add big organization generator
- Add pennon generation for heraldry
- Reorganize organization and heraldry classes into new namespaces

## 1.3.0 (2020-09-08)

- Replaced the third-party S3 facade with the Laravel core one
- Pull heraldry charge mask and line images from Digital Ocean and cache locally

## 1.2.3 (2020-09-06)

- Fix bend sinister division

## 1.2.2 (2020-09-06)

- Add Open Graph image metadata to layout
- Try to fix input font size and family

## 1.2.1 (2020-09-06)

- Fix login page

## 1.2.0 (2020-09-06)

- Fix sable charges not having a border
- Fix variations of the field not supporting furs
- Add ability for user to customize heraldry
- Add 21 more charges
- Add heraldry to organizations
- Give organizations more traits
- Add page showing all heraldic charges

## 1.1.0 (2020-09-02)

- Rewrite the heraldry API from Go to PHP
- Remove ordinaries temporarily from heraldry
- Add "in chief" and "in fess" charge locations
- Add variant arrangements of multiple charges

## 1.0.4 (2020-08-26)

- Fix the wrapping on the nav bar and columns

## 1.0.3 (2020-08-25)

- Fix image file extensions, for real this time

## 1.0.2 (2020-08-25)

- Fix image file extensions

## 1.0.1 (2020-08-25)

- Restore missing CSS

## 1.0.0 (2020-08-25)

- Rewrite almost the _entire_ World API as part of this application
- Redesign everything
- Have a beer

## 0.6.6 (2020-04-20)

- Add the same cache bug fix to culture and region

## 0.6.5 (2020-04-20)

- Fix a typo in the heraldry controller introduced in 0.6.4

## 0.6.4 (2020-04-20)

- Fix a bug where heraldry that existed but wasn't in the cache would cause an error

## 0.6.3 (2020-04-19)

- Add cyberpunk chop shop generator to the quick generators page
- Update a ton of dependencies

## 0.6.2 (2020-04-09)

- Add Plausible tracking snippet

## 0.6.1 (2020-03-18)

- Change templates to support v0.11.0 of World API

## 0.6.0 (2020-01-19)

- Save rendered HTML of generated regions and cultures
- Change displays to use saved rendered HTML
- Fix language description display
- Fix description of religion in culture display
- Remove PDF save for cultures (for now)
- Remove deprecated Docker development workflow

## 0.5.2 (2020-01-15)

- Fix display of cultures

## 0.5.1 (2020-01-14)

- Update HeraldryGenerator to pull from world API 0.9.0 or later

## 0.5.0 (2020-01-12)

- Add user accounts
- Add ability to save results to the database for cultures, regions, and heraldry
- Add blog articles to front page
- Add personal dashboard
- Change style of navigation bar to emphasize active page
- Center content on heraldry display page
- Add "most recent" to culture, region, and heraldry pages
- Add field shape selection to heraldry generator

## 0.4.2 (2019-12-02)

- Add Fathom site ID

## 0.4.1 (2019-12-02)

- Make Fathom domain configurable

## 0.4.0 (2019-09-13)

Upgrade to Laravel 6

### Change List

- Upgrade to Laravel 6
- Switch to using phpredis instead of predis
- Fix remaining bugs in heraldry references

## 0.3.1 (2019-09-13)

### Change List

- Fix a bug in the heraldry router

## 0.3.0 (2019-09-13)

### Change List

- Make use of the new heraldry API
- Update dependencies
- Update to PHP 7.3
- Update to Node.js 12.10

## 0.2.8 (2019-09-03)

### Change List

- Make Redis port and password ENV configurable

## 0.2.7 (2019-07-21)

### Change List

- Fix food and drink display

## 0.2.6 (2019-07-21)

### Change List

- Updated composer dependencies
- Updated npm dependencies

## 0.2.5 (2019-07-01)

### Change List

- Support the new naming language feature of 0.2.2 of the API

## 0.2.4 (2019-06-30)

### Change List

- Update to use the new 0.2.3 version of the World API
- Fix some small display bugs

## 0.2.3 (2019-06-29)

### Change List

- Fix typo in CSS that prevented body font from loading

## 0.2.2 (2019-06-28)

For archival purposes, I added PDF generation for cultures so you can locally save your results.

### Change List

- Add PDF generation for cultures

## 0.2.1 (2019-06-26)

This is a typography release.

### Change List

- Add website icons
- Increase font size across the board
- Replace typefaces with new ones

## 0.2.0 (2019-06-26)

This version moves to using manual semantic versioning. The previous
scheme used 0.1.X versioning, where X was the CircleCI build number.

The version number 0.2.0 was chosen to differentiate it from that
process.

### Change List

- Tag the current version of the Iron Arachne website as v0.2.0
- Update the CI/CD configuration for CircleCI to reflect the new scheme
- Add the git commit hash as an additional Docker image tag
